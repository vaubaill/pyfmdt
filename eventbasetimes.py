#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 10:24:36 2025

@author: vaubaill
"""
from astropy.time import Time
import astropy.units as u
import numpy as np



# recording_2024-12-12_20-40-59.avi
t=Time('2024-12-12T20:40:59')
fps=30/u.s
frames=np.array([5710,27062,59477])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)

# recording_2024-12-12_21-28-19.avi
t=Time('2024-12-12T21:28:19')
fps=60/u.s
frames=np.array([97,29664,38016,38465])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)

# recording_2024-12-12_23-01-44.avi
t=Time('2024-12-12T23:01:44')
fps=30/u.s
frames=np.array([15990])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)

# recording_2024-12-12_23-17-42.avi
t=Time('2024-12-12T23:17:42')
fps=30/u.s
frames=np.array([2610])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)
    
# recording_2024-12-12_23-21-51.avi
t=Time('2024-12-12T23:21:51')
fps=30/u.s
frames=np.array([3926,14767])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)

# recording_2024-12-13_00-58-23.avi
t=Time('2024-12-13T00:58:23')
fps=30/u.s
frames=np.array([14767])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)


# recording_2024-12-13_01-01-22.avi
t=Time('2024-12-13T01:01:22')
fps=30/u.s
frames=np.array([35128,52100,54450,64547])
times=t+frames/fps - 1*u.hr
for tt in times:
    print(tt.isot)



