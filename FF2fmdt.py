#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 08:47:39 2023

@author: vaubaill
"""
import os
import shutil
import glob
import argparse


import img2img, launch_fmdt
from fmdt_log import log
from Utils import FFtoFrames

home = os.getenv('HOME') + '/'



# FF_dir = '/home/vaubaill/OBS-Campaign/2023-MALBEC/2023EtaAquarids/2023_05_04_EtaAquarids_Aire/cam_01/FR2001_20230505_020221_315459/'
# FF_dir = '/media/ccolomer/Expansion/TAH2022/PH0002/RMS_data/CapturedFiles/PH0002_20220525_153053_404512/'
# FMDT_dir = '/media/ccolomer/Expansion/TAH2022/PH0002/FMDT_data/fmdt/'
#RMS_dir = '/RMS_data/CapturedFiles/'
tmp_dir = home + '/tmp/'


def FF2FMDT(FF_dir,FMDT_dir,tmp='/tmp/',pgm='/pgm/',pat=None,wrt=None, \
            fmdt_conf_temp=home+'/pyfmdt/conf/config_template_Basler1920.in'):
    """
    

    Parameters
    ----------
    FF_dir : string
        RMS direrctory where fits files are located.
    FMDT_dir : string
        Output directory.
    tmp : string, optional
        Temporary sub-directory where individual png frames will be stored.
        Default if '/tmp/'.
    pgm : string, optional
        Temporary dsub-irectory where individual pgm frames will be stored.
        Default if '/pgm/'.
    fmdt_conf_temp : string, optional
        fmdt_solve template configuration file name.
        Default is home+'/pyfmdt/conf/config_template_Basler1920.in'.
    RMS_dir : string, optional
        RMS data sub-directory. Can be empty string.
        Default is '/RMS_data/CapturedFiles/'.
    pat : string, optional
        Pattern of fits files to be processed. Default is None, meaning 
        all fits files in FF_dir will be processed.
    wrt : string, optional
        If not None, name of the file where launch_fmdt commands are written
        instead of running launch_fmdt. This is useful when conflicts between
        python versions of RMS and FMDT occurs.
        Default is None, meaning that launch_fmdt will be luanched.

    Returns
    -------
    None. 

    """
    # set temporary directories
    tmp_dir = FMDT_dir + tmp
    pgm_dir = FMDT_dir + pgm
    log_dir = FMDT_dir + 'log/'
    tmp_vis = tmp_dir + 'visu/'
    for p in [FMDT_dir,tmp_dir,pgm_dir]:
        if not p.endswith('/'):
            p = p + '/'
        if not os.path.isdir(p):
            log.info('### creating directory: '+p)
            os.mkdir(p)
    # get list of fits files in FF_dir
    if pat:
        pattern = FF_dir+pat+'*.fits'
    else:
        pattern = FF_dir+'*.fits'
    listfits = glob.glob(pattern)
    listfits.sort()
    log.info('There are '+str(len(listfits))+' files of type '+pattern)
    # loop over fits files
    for FF_file in listfits:
        log.info('*** Now processing fits: '+FF_file)
        # clean tmp_dir and pgm_dir
        for dir2clean in [tmp_vis,tmp_dir,pgm_dir,log_dir]:
            if os.path.isdir(dir2clean):
                log.warning('Cleaning directory: '+dir2clean)
                os.chdir(dir2clean)
                for f in os.listdir(dir2clean):
                    if os.path.isfile(f):
                        os.remove(f)
        
        # convert FF to frames
        FFtoFrames.FFtoFrames(FF_file, tmp_dir, 'png', -1)
        
        # convert png into pgm
        root = os.path.basename(FF_file).split('.')[0] + '_'
        img2img.img2img(tmp_dir,pgm_dir,'png','pgm',rename=root,visu_pth=tmp_vis)
        
        # run fmdt
        if wrt:
            with open(wrt,'r') as f:
                cmd = 'python launch_fmdt -i '+ pgm_dir + \
                      ' -o ' + FMDT_dir + \
                      ' -p ' + root + \
                      ' -x pgm ' + \
                      ' -c ' + fmdt_conf_temp + \
                      ' -img '
                f.write(cmd)
            log.info('launch_fmdt cmd saved in: '+wrt)
        else:
            launch_fmdt.launch_fmdt(pgm_dir,FMDT_dir,root,'pgm','',fmdt_conf_temp,imgseq=True)

if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='FF2fmdt arguments.')
    parser.add_argument('-i',default='./',help='Input data directory path name. Default is: ./.')
    parser.add_argument('-o',default='./FMDT/',help='Output subdirectory. Default is: ./FMDT/')
    parser.add_argument('-s',default=None,help='Subdirectory input path pattern. If not None, loop over input directory sub-directories will be processed.  Default is: None meaning there is no input subdirectory.')
    parser.add_argument('-p',default=None,help='Fits file pattern. Default is: None meaning all fits files will be processed.')
    parser.add_argument('-c',default=home+'/pyfmdt/conf/config_template_Basler1920.in',help='fmdt_solve template configuration file. Default is: home+/pyfmdt/conf/config_template_Basler1920.in')
    parser.add_argument('-w',default=None,help='If not None and a string, file name containing the command to launch_fmdt. Useful for incompatible python versions between RMS and FMDT. Default is: None.')
    
    args = parser.parse_args()
    
    # set arguments
    pthi = args.i
    subi = args.s
    ptho = args.o
    pat = args.p
    cfg = args.c
    wrt = args.w
    
    # refine variable
    for d in [pthi,ptho]:
        if not d.endswith('/'):
            d = d + '/'
        
    # launch
    # manage input data sub-directories
    if subi:
        pattern = pthi + subi + '*'
    else:
        pattern = pthi
    list_dir = glob.glob(pattern)
    list_dir.sort()
    log.debug(str(list_dir))
    log.info('=== There are '+str(len(list_dir))+' directories of type '+pattern+' to process')
    # loop over FF directories
    for FF_dir in list_dir:
        log.info('*** Processing directory: '+FF_dir)
        if not FF_dir.endswith('/'):
            FF_dir = FF_dir + '/'
        FF2FMDT(FF_dir,ptho,pat=pat,wrt=wrt,fmdt_conf_temp=cfg)
    
else:
    log.debug('successfully imported')


log.info('done')
