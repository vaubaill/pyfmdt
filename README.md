# pyFMDT

Suite of tools to exploit the [FMDT software](https://github.com/alsoc/fmdt) output.

## What is pyFMDT?

pyFMDT was created to exploit the results provided by the [Fast Meteor Detection Tool](https://github.com/alsoc/fmdt).
It performs astrophotometry computations and outputs the position and magnitude of each detected meteor as a function of time.

## List of tools

- fmdt_reduce.py: deprecated: do not use
- fmdt_solve.py: performs meteor reduction: solves for astrophotometry and outputs position and magnitude as a function of time. This is the core of the package.
- launch_fmdt.py: automatically launches FMDT, then fmdt_solve.py, for either a serie of videos or image sequences. A configuration file is automatically created for fmdt_solve. Arguments are provided in the command line. See: python3 launch_fmdt.py --help for a list of arguments. In a perfect world, this is the only script you need to run.
- fmdt_read.py: read FMDT output files
- video_fmdt2frame.py: uses ffmpeg to extract meteor frames. Useful for visualization purpose and for individual frame astrometry reduction.
- imgseq_track2frame.py: convert meteor frames to png (or other format) images. Useful for visualization purpose.
- img2img.py: convert original image sequence into pgm format image sequence. Note: FMDT requires pgm files for image sequence.
- img2video.py: converts image sequence into a serie of fixed duration videos. Useful fir visualization purpose.
- tah22-putposition.py : tuned for the 2022 tau-Herculids mission: put airplane GPS position in RMS configuration files.

## Dependencies

pyFMDT makes extensive use of [astropy](https://www.astropy.org/) tools, including [astroquery](https://astroquery.readthedocs.io/en/latest/gaia/gaia.html).
pyFMDT also uses [ffmpeg](https://ffmpeg.org/) to extract frames from videos, [photutils](https://photutils.readthedocs.io/en/stable/) and [sklearn](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.RANSACRegressor.html) for photometry purpose.

pyFMDT uses the [Astrometry.net engine](https://astrometry.net/doc/readme.html) that must be installed locally to performs the astrometry reduction.


## How to use pyFMDT.

Ideally, you run the launch_fmdt.py script with all necessary options to both run FMDT and the fmdt_solve.py scripts.
The syntax is:
	python3 launch_fmdt.py -i input -o output -p pattern [-x extension -img -r root -c config_template -fx fmdt-detect -fv fmdt-visu --nofmdt --nosolve --noXtract --params]
with:
- input: Input data path. Default is: './'
- output: Output data path. Default is: './'
- pattern: Video or Images file pattern. Example 1: p="Basler" if all video files starts with "Basler_". Example 2: for an image sequence stored in /my/path/, then p="image_" if pgm files are of pattern /my/path/image_*.pgm. Default is empty string.
- extension: Input file extension. Ex: .mkv  for video, or .pgm for image sequence. Default is .mkv
- img: If set, image sequence is considered, rather than video. Default is False.
- root: FMDT output file root name. Default is empty string.
- config_template: fmdt_solve configuration file template full path name. Default is: $home/fmdt/pyFMDT/conf/config_template.in.
- fmdt-detect: FMDT-detect full path executable file. Default is: $home/fmdt/build/exe/fmdt-detect-rt-pip
- fmdt-visu: FMDT-visu full path executable. Default is: $home/fmdt/build/exe/fmdt-visu
- nofmdt: If set, FMDT is not launched. Useful for debug purpose.
- noXtract: If set, the solve-field script is not launched. Useful for debug purpose.
- params: fmdt executable parameters key-value pairs, space separated. Example: key1=value1 key2=value2 etc.

The later documentation focuses on fmdt_solve.py and assumes you have successfully processed your video thanks to [FMDT](https://github.com/alsoc/fmdt).
fmdt_solve.py uses a configuration file to provide all desired options.

fmdt_solve.py is launched on a Terminal using the syntax:
	python3 fmdt_solve.py -c config_file
with:
- config_file: the pyFMDT [configuration file](#header-configuration-file).

## Configuration file.

The configuration file allows the user to provide all necessary information and options. Here is an example:

	[USER]
	# data directory
	data_dir = /Volumes/Expansion/TAH2022/test_mond/results/
	# data type: 'video' (video file) or 'img_seq' (image sequence). If img_seq an IMGSEQ section MUST exist.
	type = img_seq
	# Input Video or image file name pattern
	pattern = W3_04_59_18
	# input file (video or image) format or extension. Example: mp4 for video, %5d.pgm for image sequence.
	fmt = %5d.pgm
	# output file directory: will contain the meteor astrometry and light curve.
	out_dir = $data_dir/fmdt_reduced/
	# extracted meteor frames directory: this is where individual frames processed data will go. Advice is to set a subdirectory to out_dir.
	frame_dir = $out_dir/meteor_frames/
	# log level
	log_level = DEBUG
	# astrometry.net solve-field script configuration file
	astrometry_cfg = /home/vaubaill/PROJECTS/PODET/PODET-MET/pyfmdt/conf/astrometry-WFOV.cfg
	# output file root name
	root = fmdt_out
	
	[CAMERA]
	# sensor width and height in [pixels]
	image_width = 3840
	image_height = 2160
	# downsampling factor for the solve-field script. This is especially useful for large images
	# because solve-field may find thousand of sources. Tune it to have about 100 sources found.
	# If not set, no downsampling is performed.
	dwnsmpl = 6
	# Field of view size in [deg]
	fov_width = 27.0
	fov_height = 16.0
	# estimtated limiting magnitude
	LM = 8.0
	# camera frame per second, in [Hz]
	fps = 25.0
	# video time start
	time_start = 2022-05-31T05:00:00.000
	# Photometric band. choice is 'V' (visible), 'R' (red), 'G' (green), 'B' (Blue)
	band = V
	
	[PROCESS]
	# Flux average over n frames (default is 0).
	avgflx = 5
	# star match tolerance factor (while comparing solve-field output and catalog query), in [pixel]
	match_tol_fct = 3.0
	# minimum number of stars to be detected to perform the astrometry reduction
	min_star_nb = 15
	# option: read existing astrometry data: useful for debug purpose only. Default is False.
	read_astrometry = False
	# option: read existing photometry data: useful for debug purpose only. Default is False.
	read_photometry = False
		
	[FMDT]
	# Note: should yo uuse the the launch_fmdt.py script, this section will be automatically updated.
	# Object tracking file name
	track_file = ${USER:data_dir}/track.dat
	# Bounding box file name
	bb_file = ${USER:data_dir}/bb.dat
	# magnitude file name
	mag_file = ${USER:data_dir}/mag.dat
	# json file
	json_file = ${USER:data_dir}/frames.json
	# output log directory containing frame by frame info
	log_dir = ${USER:data_dir}/log
	# meteor-min: value of the FMDT --trk-meteor-min parameter (see FMDT documentation).
	trk-meteor-min = 3
	
	
## What does pyFMDT/fmdt_solve do, and how does it do it?

pyFMDT/fmdt_solve.py first reads FMDT output files, thanks to the fmdt_read package.
It searches for 'meteor' labeled objects in the FMDT track file.
For each meteor, it loops over each frame and performs an astrophotometry reduction.
For this, the locally installed solve-field from Astrometry.net is used.
Then the Vizier service is queried to retrieve all known stars in the field of view.
The currently used star catalog is Hipparcos and is retrieved using the [astroquery.GAIA](https://astroquery.readthedocs.io/en/latest/gaia/gaia.html) package.
The output is a catalog (list) of known stars, including their magnitude in different bands.
The later is matched to the stars detected in the image (depending on the band).
The star match is performed at the pixel level by default. However, it may be extended to the value set by the match_tol_fct keyword of the configuration file (see the PROCESS section).
A fit between the flux (in [ADU]) and V-magnitude of known stars is performed (using the RANSAC algorithm).
This fit is used to compute the magnitude of the meteor in the chosen band, for each frame.
The operation is repeated for each frame, and for each meteor.

## pyFMDT/fmdt_solve.py output files

pyFMDT/fmdt_solve.py produces several files, located in the output directory specified in the configuration file (in the USER section).
The USER:data_dir directory will contain:
- $root_meteor_$meteorid_LC.png : meteor light curve graph in png format. The meteorid is the meteor id defined by FMDT.
- $root_meteor_$meteorid.dat : all astrophotometry data for meteor $meteorid: image and sky coordinates, flux and magnitude estimates.

Other file are saved in the USER:frame_dir directory:
- $root_photfit_$meteorid_$frame.png : photometry fit for meteor id=$meteorid in frame $frame.
- $root_$meteorid_$frame.fits : Frame (may be extracted from the video file using ffmpeg) in fits format.
- $root_$meteorid_$frame-calib.fits : Calibrated frame with astrometry data (in header) in fits format.
- $root_star_catalog_$frame.dat : stars catalog queried for frame $frame.
- $root_star_matched_$frame.dat : matched stars between solve-field and catalog stars.
- $root_$meteorid_$frame-astrometry.dat : astrometry data: image coordinates, sky coordinates, flux, as estimated with photutils tools.
- $root_$meteorid_$frame-corr.fits : solve-field corr output file including the image and sky coordinates of sources.
- $root_$meteorid_$frame-corr.dat : same as $root_$meteorid_$frame-corr.fits, in text format.
- $root_$meteorid_$frame-header.txt : solve-field header output file.
- $root_$meteorid_$frame-axy.fits : solve-field axy output file.
- $root_$meteorid_$frame-match.fits : solve-field match output file.
- $root_$meteorid_$frame-match.fits : solve-field match output file.
- $root_$meteorid_$frame-match.fits : solve-field match output file.
- $root_$meteorid_$frame-rdls.fits : solve-field rdls output file.
- $root_$meteorid_$frame-solve : solve-field solve output file.
- $root_$meteorid_$frame-solve.txt : solve-field standard output file name: this is where the center and size of the field of view are retrieved from.
- $root_$meteorid_$frame-pnm.bin : solve-field pnm output file.
- $root_$meteorid_$frame-xyls.fits : solve-field xyls output file.
- $root_$meteorid_$frame-indx.png : solve-field index image output file.
- $root_$meteorid_$frame-ngc.png : solve-field ngc image output file.
- $root_$meteorid_$frame-objs.png : solve-field objects image output file.

## How to use pyFMDT/fmdt_solve output files?

### Meteor output:

- \$root_meteor_\$frame.dat may be used for 3D-trajectory and orbit calculation provided the same meteor was observed with another camera from another location.
- \$root_meteor_$id_LC.png: meteor light curve.
- USER:frame_dir subdirectory: includes all individual meteor frames: useful for debug or additional data analysis purpose.

### FMDT output: only if the launch_fmdt.py script is used.
If the launch_fmdt.py script is used to launch the FMDT suite (detection and visualization), the output directory also contains:
- \$root_visu.mp4 file: FMDT visualization video file.
- \$root_detect.mp4 file: FMDT detection video file.
- \$root_track.dat: FMDT track file.
- \$root_trk-roi.dat: FMDT track roi file.
- \$root_bb.dat: FMDT bb file.
- \$root_mag.dat: FMDT magnitude file.
- \$root_config.in: fmdt_solve.py configuration file (automatically geenrated).
