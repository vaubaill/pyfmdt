#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 08:58:41 2025

@author: vaubaill
"""

import os
import numpy as np
from PIL import Image

def average_images_in_directory(directory_path):
    # Get all image files in the directory
    image_files = [f for f in os.listdir(directory_path) if f.endswith(('.png', '.jpg', '.jpeg'))]
    
    if not image_files:
        raise ValueError("No images found in the directory.")
    
    # Open the first image to get its size and mode
    first_image_path = os.path.join(directory_path, image_files[0])
    first_image = Image.open(first_image_path)
    
    # Convert the first image to a numpy array
    avg_array = np.array(first_image, dtype=np.float32)
    
    # Sum the pixel values of all the images
    for image_file in image_files[1:]:
        image_path = os.path.join(directory_path, image_file)
        img = Image.open(image_path)
        
        # Ensure the image has the same size and mode
        if img.size != first_image.size or img.mode != first_image.mode:
            raise ValueError(f"Image {image_file} has a different size or mode than the first image.")
        
        avg_array += np.array(img, dtype=np.float32)
    
    # Compute the average by dividing by the number of images
    avg_array /= len(image_files)
    
    # Clip values to valid range for the image type (0-255 for RGB)
    avg_array = np.clip(avg_array, 0, 255).astype(np.uint8)
    
    # Convert the averaged array back to an image
    avg_image = Image.fromarray(avg_array)
    
    return avg_image

def save_average_image(directory_path, output_filename="average_image.png"):
    avg_image = average_images_in_directory(directory_path)
    output_path = os.path.join(directory_path, output_filename)
    avg_image.save(output_path)
    print(f"Average image saved to {output_path}")

# Example usage:
directory = "/home/vaubaill/PROJECTS/PODET/PODET-MET/pyfmdt/20241212T203633/diff/"
save_average_image(directory, "average_image.png")
