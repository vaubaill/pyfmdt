[USER]
# home
home = auto
# data directory
data_dir = /Volumes/Expansion/TAH2022/test_mond/results/
# data type: 'video' (video file) or 'img_seq' (image sequence). If img_seq an IMGSEQ section MUST exist.
type = img_seq
# File name pattern
pattern = Basler_acA1920-155um__23278758__20220531_042259204_
# input file (video or image) format
fmt = %5d.pgm
# output file directory
out_dir = $data_dir/fmdt_reduced/
# extracted meteor frames directory: see also extract_frame.py
frame_dir = $out_dir/meteor_frames/
# log level
log_level = DEBUG
# astrometry.net configuration file
astrometry_cfg = ${home}/PROJECTS/PODET/PODET-MET/pyfmdt/conf/astrometry-WFOV.cfg
# astrometry.net fits template file
astrometry_xyls = ${home}/PROJECTS/PODET/PODET-MET/pyfmdt/conf/template.xyls
# output file root name
root = GRAY_

[CAMERA]
# sensor width and height in [pixels]
image_width = 3840
image_height = 2160
# FOV size in [deg]
fov_width = 27.0
fov_height = 16.0
# estimtated limiting magnitude
LM = 8.0
# camera frame per second, in [Hz]
fps = 25.0
# approximate center of field of view: right Ascension in HH:MM:SS and Declination in DEG
#RA = 11:56:01
#DEC = -27:44:49
# video time start
time_start = 2022-05-31T05:00:00.000
# Photometric band. choice is 'V' (visible), 'R' (red), 'G' (green), 'B' (Blue)
band = V

[PROCESS]
# Flux average over n frames (default is 0)
avgflx = 5
# star match tolerance factor, in [pixel]
match_tol_fct = 3.0
# minimum number of stars to be detected to perform astrometry.net astrometry reduction
min_star_nb = 15
# option: read existing astrometry data: useful for debug purpose only. Default is False.
read_astrometry = False
# option: read existing photometry data: useful for debug purpose only. Default is False.
read_photometry = False


[FMDT]
# fmdt-log-parser requires python3.9 while RMS runs python3.8. venv is the name of the conda virtual environment whre python3.9 is installed.
# venv = fmdt
# Object tracking file name
track_file = ${USER:data_dir}/track.dat
# Bounding box file name
bb_file = ${USER:data_dir}/bb.dat
# magnitude file name
mag_file = ${USER:data_dir}/mag.dat
# json file
json_file = ${USER:data_dir}/frames.json
# output log directory containing frame by frame info
log_dir = ${USER:data_dir}/log
# meteor-min: value of the --trk-meteor-min parameter
trk-meteor-min = 3
