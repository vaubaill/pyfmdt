#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 11:32:43 2023

@author: A. Pautova, J. Vaubaillon, IMCCE, 2023
"""

import os
import glob
import argparse
import logging

from fmdt_log import log
log.setLevel(logging.DEBUG)

def img2img(pth_in,pth_out,fmti,fmto,\
            rename=None,visu_pth=None, gamma=1.0):
    """ Convert the images to the correct pattern (change format and rename)

    Parameters
    ----------
    pth_in : string
        path to the folder containing the images directory.
    pth_out : string
        Output path where images will be saved.
    fmti : string
        format of input image.
    fmto : string
        format of output image.
    rename : string, optional
        if not None and a string, image file name is renamed 'img_%05d.'+fmto. Default is None.
    visu_pth : string, optional
        If set, a copy of the normalized images are saved in visu_pth. 
        This is useful for visualization purpose. Default is None.
    gamma : flot, optional
        If not 1.0, set gamma value for conversion. Default is 1.0.
    
    Returns
    -------
    None.

    """
    # check that the paths do exist and ends with slash
    if not os.path.exists(pth_in):
        msg = '*** FATAL ERROR: path does not exist: '+pth_in
        log.error(msg)
        raise IOError(msg)
    # set pth_out
    if not pth_in.endswith('/'):
        pth_in = pth_in + '/'
    if not pth_out.endswith('/'):
        pth_out = pth_out + '/'
    if not os.path.exists(pth_out):
        msg = 'Create pth_out: '+pth_out
        log.warning(msg) 
        os.mkdir(pth_out)
    if visu_pth is not None:
        if not visu_pth:
            visu_pth = None
        else:
            if not visu_pth.endswith('/'):
                visu_pth = visu_pth + '/'
            if not os.path.exists(visu_pth):
                msg = 'Create visu_pth: '+visu_pth
                log.warning(msg) 
                os.mkdir(visu_pth)
    
    # log
    log.debug('pth_in : '+pth_in)
    log.debug('pth_out : '+pth_out)
    log.debug('fmti : '+fmti)
    log.debug('fmto : '+fmto)
    log.debug('visu_pth : '+str(visu_pth))
    
    # save current directory and change directory
    curdir = os.getcwd()
    log.info('Relocating to '+pth_in)
    try:
        os.chdir(pth_in)
    except:
        msg = 'FATAL ERROR: unable to change to directory '+pth_in
        log.error(msg) 
    
    # list of images
    img_root = '*'+fmti
    img_list = glob.glob(img_root)
    img_list.sort()
    log.info('There are '+str(len(img_list))+' images of pattern '+img_root)
    if rename:
        # if rename is a string
        try:
            if len(rename):
                root = rename
        # if rename is True
        except:
            root = os.path.basename(img_list[0]).split('.')[0] + '_'
        imgnb = 0
        log.info('rename : '+str(rename))
        log.info('root : '+str(root))
    # loop processing each image
    for img in img_list : 
        # rename the img no matter what
        img_und = img.split('_')[-1]
        #log.debug('img_und: '+img_und)
        if rename:
            img_bis = str(imgnb).zfill(5)
            new_img = root+img_bis+'.'+fmti
            new_img = new_img.replace('__','_')
            imgnb = imgnb+1
        else:
            img_bis = img_und.zfill(5)
            new_img = img.replace(img_und,img_bis)        
        keepgoing = True
        while keepgoing:
            new_img = new_img.replace('_00000_00000_','')
            if not '_00000_00000_' in new_img:
                keepgoing = False
        cmd = 'mv '+img+' '+new_img
        if img==new_img:
            msg = '*** no need to rename '+img
            log.info(msg) 
        else:
            try:
                log.debug('cmd: '+cmd)
                os.system(cmd)
            except:
                msg = 'Unable to rename '+img+' into '+new_img
                log.error(msg) 
        # build convert command
        pgm_img = pth_out + new_img.replace(fmti,fmto)
        if os.path.exists(pgm_img):
            log.info(pgm_img+' Already exists: No need to convert image '+new_img)
        else:
            cmd = 'convert '+new_img+' '+pgm_img
            if visu_pth is not None:
                cmd = cmd + ' ; convert -normalize '+new_img+' '+\
                            visu_pth+os.path.basename(pgm_img)
            log.debug('cmd: '+cmd)
            try:
                os.system(cmd)
            except:
                msg = 'Unable to convert '+new_img+' into '+pgm_img
                log.error(msg) 
    # go back to initial directory
    os.chdir(curdir)
    log.info('Back to initial directory: '+curdir)


if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='img2img.py arguments.')
    parser.add_argument('-fi', default='tiff', help='Image file extension. Default is: "tiff".')
    parser.add_argument('-fo', default='pgm', help='Output image file extension. Default is: "pgm".')
    parser.add_argument('-pi', default='/Volumes/Expansion/TAH2022/Basler_2040/', help='Path to images. Default is: /Volumes/Expansion/TAH2022/Basler_2040/')
    parser.add_argument('-po', default='/Volumes/Expansion/TAH2022/Basler_2040/Images_PGM/', help='Output path of processed images. Default is: /Volumes/Expansion/TAH2022/Basler_2040/Images_PGM/')
    parser.add_argument('-v', default='', help='Output folder for visualization purpose name. Default is empty string meaning no such image will be created.')
    parser.add_argument('-r', default=None, help='If set to "True", files are renamed to comply with FMDT format.')
    args = parser.parse_args()
    
    # 
    fmti = args.fi
    fmto = args.fo
    pi = args.pi
    po = args.po
    vis = args.v
    rnm = args.r
    
    try:
        if rnm.upper() in ['TRUE','1','Y']:
            rnm = True
    except:
        pass
    
    # launch
    img2img(pi,po,fmti,fmto,visu_pth=vis,rename=rnm)
    
else:
    log.debug('successfully imported')
              
        
    





