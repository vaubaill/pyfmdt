#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create file listing and save it to text file. Useful for directory containing a lot of files.

Created on Tue Dec  6 10:39:53 2022

@author: vaubaill
"""
import os
import glob
import numpy as np

# User input settings
# path to process
#pth = '/media/sf_Volumes/LaCie/DATA_OBS/2022-TAH/TAH2022/Basler1920/'
pth = '/media/sf_Volumes/Expansion/TAH2022/Basler_2040/FMDT_res/'
#pth = './'
# path pattern to list
#dir_pat = 'Basler'
dir_pat = 'meteor'
# image pattern to list
img_pat = 'Basler'
img_ext = 'png'

# Main program
curdir = os.getcwd()
# change directory
os.chdir(pth)


# make list if files and directoris in current directory
list_dir = glob.glob(dir_pat+'*[!txt]')
print('list_dir: '+str(list_dir))

for dir2list in list_dir:
    print('*** processing directory: '+dir2list)
    if not os.path.isdir(dir2list):
        print('=== Warning: '+dir2list+' is not a directory: skipped...')
        continue
    filo = dir2list.replace('/','')+'-list.txt'
    print('Listing will be saved in '+filo)
    print('Listing files...')
    #list_file = list_dir # TODO: REMOVE
    list_file = np.array(glob.glob(dir2list+'/'+img_pat+'*'+img_ext))
    # sort listing if number of files > 10000
    if list_file.size > 9999:
        print('Sorting list of file')
        list_num = np.array([])
        for fname in list_file:
            list_num = np.append(list_num,int(fname.split('_')[-1].split('.')[0]))
        ids = np.argsort(list_num)
        list_file = list_file[ids]
    # save file listing in text file
    print('Saving list into '+filo+' (please wait...)')
    with open(filo,'w') as f:
        for fname in list_file:
            f.write(os.path.basename(fname)+'\n')
    print('Directory listing saved in '+filo)
    
    
# go back to initial directory
os.chdir(curdir)









