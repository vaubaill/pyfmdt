#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Launch FMDT for a serie of videos in a given directory.

Created on Wed Dec  7 16:41:06 2022

@author: J. Vaubaillon, A. Pautova, IMCCE, 2023
"""

import os
import glob
import argparse
from configparser import ConfigParser, ExtendedInterpolation
import subprocess

from fmdt_log import log
import fmdt_solve
from video_fmdt2frame import video_track2frame
from imgseq_track2frame import imgseq_track2frame
import fmdt_read


# get $HOME environment variable
home = os.getenv('HOME') + '/'

# FMDT installation path
fmdt_pth = home+'/fmdt/build/bin/'
# FMDT executables
fmdt_exe = {'detect':fmdt_pth+'fmdt-detect-rt-pip',
            'visu':fmdt_pth+'fmdt-visu',
            'log-parser':fmdt_pth+'fmdt-log-parser',
            'maxred':'fmdt-maxred'}
# detection parameters
detect_params = {'--vid-in-path':'./meteor.mp4',   # or path/basename_%05d.jpg
                 '--log-path':'log/',
                 '--cca-mag ':'',           # no parameter option
                 '--trk-roi-path':'trk-roi.txt', 
                 '--vid-out-path':'meteor_detected.mp4',
                 '--vid-out-id':'',         # no parameter option
                 '--trk-all':'', # tracks all objects: useful for astrometry
                 '--trk-meteor-min':3,
                 '--trk-star-min ':5,
                 }

# log parameters
log_params = {'--log-path':'log/',
              '--trk-roi-path':'trk-roi.txt',
              '--log-flt':'^[0-9]{5}.txt',
              '--ftr-name':'mag',
              '--ftr-path':'mag.txt',
              '--trk-path':'tracks.txt',
              '--trk-bb-path':'bb.txt',
              }
# visualization parameters
visu_params = {'--vid-in-path':'meteor.mp4',   # or path/basename_%05d.jpg
               '--vid-out-path':'meteor_visu.mp4',
               '--vid-in-threads':'0',          # 0 means ffmpeg chooses the # of threads
               '--trk-path':'tracks.txt',
               '--trk-bb-path':'bb.txt',
               '--trk-id':'',                   # no parameter option
               '--trk-nat-num':'',              # no parameter option
               '--vid-out-id':'',               # no parameter option
#               '--trk-only-meteor':'',          # no parameter option
               }


def check_file(filename):
    """Check existence of a file.
    Usefull especially for the several configuration files.
    Raise IOError if file does not exist.
    
    Parameters
    ----------
    filename : string
        Full path name of the file to check.
    
    Returns
    -------
    None.
    
    
    """
    if not os.path.exists(filename):
        msg = '*** fatal error: file '+filename+' does not exist.'
        log.error(msg)
        raise IOError(msg)
    log.debug(filename+' ok')
    return


def submitt_fmdt(cmd,crash=False,nosub=False,venv=''):
    """Submitt FMDT command.
    
    Parameters
    ----------
    cmd : string
        FMDT command to be submitted.
    crash : Boolean, optional
        If True, the program raises a SystemError if FMDT does not work. Default is False.
    nosub : Boolean, optional
        If True the command is not run. Useful for debug purpose. Default is False.
    venv : string, optional
    	If not empty string, name of a conda virtual environment needed to run the script.
	Default is empty string.
    
    Returns
    ------
    None.
    """
    wngmsg = 'Continuing with next set of data (video or images)'
    if venv:
        cmd = 'conda run -n ' + venv +' '+ cmd    
    log.debug('Submit cmd: '+cmd)
    
    # if nosub, just print msg and return
    if nosub:
        msg = 'Command not submitted on purpose!'
        log.warning(msg)
        return
    
    # submit command
    try:
        #os.system(cmd)
        if venv is not None:
            subprocess.run(cmd, shell=True, check=True)
        else:
            os.system(cmd)
    except subprocess.CalledProcessError:
        msg = 'Problem with the FMDT command: '+cmd
        log.error(msg)
        raise SystemError(msg) if crash else log.warning(wngmsg)
    except:
        msg = 'FMDT cmd unable to go through: '+cmd
        log.error(msg)
        raise SystemError(msg) if crash else log.warning(wngmsg)
    return

def launch_fmdt(pth_in,pth_out,fil_pat,ext,out_root,config_template,\
                imgseq=False,nofmdt=False,nosolve=False,noXtract=False,
                exe_params={},visu_pth=None):
    """Check and create directory of usefuls files before launching fmdt.

    Parameters
    ----------
    pth_in : string
        Path of all video files, or path to image series main folder.
    pth_out : string
        Output path.
    fil_pat : string
        Video or Images file pattern. Ex: "Basler".
    ext : string
        Video or Image file extension.
    out_root : string
        FMDT output file root name.
    config_template : string
        fmdt_solve configuration file template.
    imgseq : Boolean, optional
        If True, image sequence rather than video files are searched for.
        Default is False.
    nofmdt : Boolean, optional
        If True the FMDT command is not run. Useful for debug purpose. Default is False.
    nosolve: Boolean, optional
        If True the solve-field command is not run. Useful for debug purpose. Default is False.
    noXtract : Boolean, optional
        If True the frame extraction is not performed. Useful for debug purpose. Default is False.
    exe_params : Dict
        dictionnary of FMDT executable (detect, log or visu) parameters.
    visu_pth : string, optional
        If set and video file is used, visu_pth video file is used for visualization.
        If set and image sequence is used, visu_pth is the directory where normalized
        images are located. Default is None meaning that input video file or image sequence
        is used for visualization purpose.
        
    Returns
    -------
    None.

    """    
    # debug
    log.debug('pth_in : '+pth_in)
    log.debug('pth_out : '+pth_out)
    log.debug('fil_pat : '+fil_pat)
    log.debug('ext : '+ext)
    log.debug('out_root : '+out_root)
    log.debug('config_template : '+config_template)
    log.debug('imgseq : '+str(imgseq))
    log.debug('exe_params : '+str(exe_params))
    log.debug('detect_params : '+str(detect_params))
    
    # go to video directory
    curdir = os.getcwd()
    log.info('Change directory: '+pth_in)
    os.chdir(pth_in)
    
    # check files and directories
    check_file(fmdt_exe['detect'])
    check_file(fmdt_exe['log-parser'])
    check_file(fmdt_exe['visu'])
    check_file(pth_in)
    check_file(config_template)
    
    # cosmetic tasks
    if not pth_in.endswith('/'):
        pth_in = pth_in + '/'
    if not pth_out.endswith('/'):
        pth_out = pth_out + '/'
    if not os.path.isdir(pth_out):
        log.info('Make output directory '+pth_out)
        os.mkdir(pth_out)
    
    # detedermine if data are in video or image serie format
    if imgseq:
        # pattern definition
        pattern = fil_pat + '%05d.' + ext
        log.debug('pattern: '+pattern)
        # process images in pth_in directory
        process_images(pattern,config_template,exe_params,\
                           pth_in=pth_in,pth_out=pth_out,out_root=out_root,
                           nofmdt=nofmdt,noXtract=noXtract,nosolve=nosolve,visu_pth=visu_pth)
    else:
        # make video pattern and search for video files
        pattern = fil_pat+'*'+ext
        list_vid = glob.glob(pattern)
        log.info('Video files found: '+ str(list_vid))
        # check if videos were found
        if not len(list_vid):
            msg = '*** FATAL ERROR: no video file found of pattern '+pattern
            log.error(msg)
            raise IOError(msg)
        # loop over all found video files
        for vid_file in list_vid:
            log.debug('====== Now processing video '+vid_file)
            process_video(vid_file,config_template,exe_params,
                          vis_file=visu_pth,
                          pth_out=pth_out,
                          nofmdt=nofmdt,nosolve=nosolve,noXtract=noXtract)
    # go back to initial directory
    os.chdir(curdir)
    log.info('Done.')
    return 

def process_video(vid_file,config_template,exe_params,pth_in='./',pth_out='./', 
                  out_root='',trk='_track.dat',
                  bb='bb.dat',mag='mag.dat',cfg='_config.in',
                  visu='_visu',vis_file=None,
                  crash=False,nofmdt=False,nosolve=False,noXtract=False):
    """ Tune fmdt options based on an image.

    Parameters
    ----------
    vid_file : string
        name of the video file.
    config_template : string
        fmdt-reduce configuration file template.
    exe_params : Dict
        dictionnary of FMDT parameters.
    pth_in :string, optional
        Input path. Default is './'.
    pth_out :string, optional
        Output path. Default is './'.
    out_root : string, optional
        FMDT output file root name. Default is './'.
    trk : string, optional
        FMDT track output file name extension. Default is 'track.dat'.
    bb : string, optional
        FMDT box output file name extension. Default is 'bb.dat'.
    mag : string, optional
        FMDT magnitude output file name extension. Default is 'mag.dat'.
    cfg : string, optional
        fmdt_solve configuration file name extension. Default is '_config.in'.
    vis_file : string, optional
        Video file for visualization purpose. Default is None meaning that
        vid_file is used for visualazation purpose.
    visu : string, optional
        FMDT output video file extension name. Default is '_visu'.
    crash : Boolean, optional
        If True and the FMDT command does not go through, raise an Error. Default is False.
    nofmdt : Boolean, optional
        If True the FMDT command is not run. Useful for debug purpose. Default is False.
    nosolve : Boolean, optional
        If True the solve-field command is not run. Useful for debug purpose. Default is False.
    noXtract : Boolean, optional
        If True the frame extraction is not performed. Useful for debug purpose. Default is False.
    
    Returns
    -------
    None.

    """
    # debug
    log.debug('vid_file : '+vid_file)
    log.debug('config_template : '+config_template)
    log.debug('pth_in : '+pth_in)
    log.debug('pth_out : '+pth_out)
    log.debug('out_root : '+out_root)
    log.debug('trk : '+trk)
    log.debug('bb : '+bb)
    log.debug('mag : '+mag)
    log.debug('cfg : '+cfg)
    log.debug('crash : '+str(crash))
    log.debug('exe_params: '+str(exe_params))
    log.debug('visu : '+str(visu))
    # make output files names
    log.debug('vid_file: '+vid_file)
    [bznm,vex] = os.path.basename(vid_file).split('.')
    common = pth_out + out_root + bznm
    # prevent empty string
    if not len(common):
        msg = '*** FATAL ERROR: input video file name is empty: '+vid_file
        log.error(msg)
        raise ValueError(msg)
    
    # meteor frame output directory
    pth_frame = pth_out+'/meteor_frame/'
    if not os.path.exists(pth_frame):
        os.mkdir(pth_frame)
    
    # output log directory
    pth_log = pth_out + '/log/'
    if not os.path.exists(pth_log):
        log.info('Making output LOG directory:'+pth_log)
        os.mkdir(pth_log)

    
    # make output file names
    track_file = common + trk
    bb_file = common + bb
    mag_file = common + mag
    conf_file = common + cfg
    if pth_out in vid_file:
        visu_file = vid_file.replace(vex,visu+'.'+vex)
    else:
        visu_file = common + visu+'.'+vex
    
    # read config file for fmdt parameters, if any
    log.info('Search for detection parameters in '+config_template)
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_template)
    config['USER']['home'] = os.getenv('HOME')
    for param in ['ccl-hyst-lo','ccl-hyst-hi','knn-d','trk-ddev']:
        log.debug('Searching for '+param)
        try:
            value = config['FMDT'][param]
            exe_params[param] = value
            log.info(param+' = '+str(value))
        except:
            log.debug(param+' not found')
            pass
    
    # build fmdt-detect command
    cmd = fmdt_exe['detect'] + ' '
    detect_params['--vid-in-path'] = vid_file
    detect_params['--vid-out-path'] = vid_file.replace('.'+vex,'_detect.'+vex)
    for prm in ['--log-path','--trk-roi-path','--vid-out-path']:
        if not pth_out in detect_params[prm]:
            detect_params[prm] = pth_out + detect_params[prm]
    all_params = ' '.join([f'{key} {value}' for key, value in detect_params.items()])
    other_params = ' '.join([f' --{key} {value}' for key, value in exe_params.items()])
    other_params.replace('True', ' ')
    cmd = cmd + all_params + other_params + ' > '+track_file
    log.info('*** Launch detect cmd: '+cmd)
    submitt_fmdt(cmd,crash=crash,nosub=nofmdt)
    
    # check if there is any meteor detected: if not, no need to submit the visu command.
    if not os.path.exists(track_file):
        log.error('*** track file '+track_file+' was not created')
        return
    try:
        trk_data = fmdt_read.read_track(track_file)
        met_trk = trk_data[trk_data['type']=='meteor']
        numet = len(met_trk)
        log.info('There are '+str(numet)+' meteors detected in '+track_file)
        if numet<1:
            log.warning('=== No meteor in '+track_file)
            return
    except:
        log.error('*** Impossible to read file: '+track_file)+' (likely nothing detected)'
        return
    
    # build the fmdt-log command
    try:
        venv = config['FMDT']['venv']
    except:
        venv = ''
    logx_params = {}
    logx_params['--log-path'] = pth_log
    logx_params['--trk-roi-path'] = detect_params['--trk-roi-path']
    logx_params['--fra-path'] = pth_log + 'frames.json'
    logx_params['--ftr-path'] = log_params['--ftr-path']
    logx_params['--trk-path'] = log_params['--trk-path']
    logx_params['--trk-bb-path'] = log_params['--trk-bb-path']
    for prm in ['--log-path','--trk-roi-path','--fra-path','--ftr-path','--trk-path','--trk-bb-path']:
        if not pth_out in logx_params[prm]:
            logx_params[prm] = pth_out + logx_params[prm]
    logx_params = {**log_params, **logx_params}
    cmd = fmdt_exe['log-parser'] + ' ' + ' '.join([f'{key} {value}' for key, value in logx_params.items()]) 
    log.info('* Launch log cmd: '+cmd)
    submitt_fmdt(cmd,crash=crash,nosub=nofmdt,venv=venv)
    
    # build fmdt-visu command
    vis_params = {}
    vis_params['--vid-in-path'] = vid_file
    vis_params['--trk-path'] = track_file
    vis_params['--trk-bb-path'] = logx_params['--trk-bb-path']
    vis_params['--vid-out-path'] = visu_file
    for prm in ['--trk-path','--trk-bb-path','--vid-out-path']:
        if not pth_out in vis_params[prm]:
            vis_params[prm] = pth_out + vis_params[prm]
    cmd  = fmdt_exe['visu'] + ' ' + ' '.join([f'{key} {value}' for key, value in vis_params.items()])
    log.info('* Launch visu cmd: '+cmd)
    submitt_fmdt(cmd,crash=crash,nosub=nofmdt)    
    
    if not noXtract:
        # extract frames from original video for astrometry purpose
        video_track2frame(pth_in+vid_file,track_file,out_pth=pth_frame,fmt='%5d.fits')
        # extract frames from visu video for visualisation purpose
        #video_track2frame(visu_file,track_file,out_pth=pth_frame,fmt='%5d.png')
    
    # create a fmdt_solve configuration file
    make_config_fmdt_solve(config_template,conf_file,pth_in,pth_out,pth_frame,
                            bznm,
                            track_file,
                            logx_params['--trk-bb-path'],\
                            logx_params['--ftr-path'],\
                            logx_params['--fra-path'],\
                            detect_params['--log-path'],\
                            nosolve=nosolve)
    
    log.debug('fmdt_solve conf_file: '+conf_file)
    # launch fmdt-reduce
    log.info('Now launching fmdt_solve with files: '+conf_file+' '+track_file+' '+bb_file+' '+mag_file)
    fmdt_solve.fmdt_solve(conf_file)
    return

def process_images(pattern,config_template,exe_params,
                   pth_in='./',pth_out='./',out_root='./res_',
                   trk='track.dat',bb='bb.dat',mag='mag.dat',
                   cfg='_config.in',crash=False,nofmdt=False,nosolve=False,
                   noXtract=False,
                   visu_pth=None):
    """Tune fmdt options based on a serie of images.
    
    Parameters
    ----------
    pattern : string
        pattern of each image file name. Ex: 'Image_%5d.pgm'.
    config_template : string
        fmdt-reduce configuration file template.
    exe_params : Dict
        dictionnary of FMDT parameters.
    pth_in :string, optional
        Input path. Default is './'.
    pth_out :string, optional
        Output path. Default is './'.
    out_root : string, optional
        FMDT output file root name. Default is './res-'.
    trk : string, optional
        FMDT track output file name extension. Default is '_track.dat'
    bb : string, optional
        FMDT box output file name extension. Default is '_bb.dat'
    mag : string, optional
        FMDT magnitude output file name extension. Default is '_mag.dat'
    cfg : string, optional
        fmdt_solve configuration file name extension. Default is '_config.in'
    crash : Boolean, optional
        If True and the FMDT command does not go through, raise an Error. Default is False.
    nofmdt : Boolean, optional
        If True the FMDT command is not run. Useful for debug purpose. Default is False.
    nosolve : Boolean, optional
        If True the solve-field command is not run. Useful for debug purpose. Default is False.
    noXtract : Boolean, optional
        If True the frame extraction is not performed. Useful for debug purpose. Default is False.
    visu_pth : string, optional
        If set, a copy of the normalized images are saved in visu_pth. 
        This is useful for visualization purpose. Default is None.
    
    Returns
    -------
    None.

    """
    # debug
    log.debug('pattern : '+pattern)
    log.debug('config_template : '+config_template)
    log.debug('pth_in : '+pth_in)
    log.debug('pth_out : '+pth_out)
    log.debug('out_root : '+out_root)
    log.debug('trk : '+trk)
    log.debug('bb : '+bb)
    log.debug('mag : '+mag)
    log.debug('cfg : '+cfg)
    log.debug('crash : '+str(crash))
    log.debug('exe_params: '+str(exe_params))
    log.debug('visu_pth : '+str(visu_pth))
    
    # make output files names
    root = pattern.split('%')[0] + '_'
    common = pth_out + out_root + root
    log.debug('common : '+common)
    track_file = common + trk
    conf_file = common + cfg
    visu_file = common +'_visu.mp4'
    
    # meteor frame and log output directory
    pth_frame = pth_out+'/meteor_frame/'
    pth_log = pth_out + '/log/'
    for pth in [pth_frame,pth_log]:
        if not os.path.exists(pth):
            log.info('Making output directory:'+pth)
            os.mkdir(pth)
    
    # read config file for fmdt parameters, if any
    log.info('Search for detection parameters in '+config_template)
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_template)
    config['USER']['home'] = os.getenv('HOME')
    for param in ['trk-meteor-min','ccl-hyst-lo','ccl-hyst-hi','knn-d','trk-ddev']:
        log.debug('Searching for '+param)
        try:
            value = config['FMDT'][param]
            exe_params[param] = value
            log.info(param+' = '+str(value))
        except:
            log.debug(param+' not found')
            pass
    
    # build fmdt-detect command
    cmd = fmdt_exe['detect'] + ' '
    detect_params['--vid-in-path'] = pth_in + pattern
    for prm in ['--log-path','--trk-roi-path','--vid-out-path']:
        if not pth_out in detect_params[prm]:
            detect_params[prm] = pth_out + detect_params[prm]
    all_params = ' '.join([f'{key} {value}' for key, value in detect_params.items()])
    other_params = ' '.join([f' --{key} {value}' for key, value in exe_params.items()])
    other_params.replace('True', ' ')
    cmd = cmd + all_params + other_params + ' > '+track_file
    log.info('*** Launch detect cmd: '+cmd)
    submitt_fmdt(cmd,crash=crash,nosub=nofmdt)
    
    # check if there is any meteor detected: if not, no need to submit the visu command.
    if not os.path.exists(track_file):
        log.error('*** track file '+track_file+' was not created')
        return
    try:
        trk_data = fmdt_read.read_track(track_file)
        met_trk = trk_data[trk_data['type']=='meteor']
        numet = len(met_trk)
        log.info('There are '+str(numet)+' meteors detected in '+track_file)
        if numet<1:
            log.warning('=== No meteor in '+track_file)
            return
    except:
        log.error('*** Impossible to read file: '+track_file+' (likely nothing detected)')
        return
    
    # build the fmdt-log command
    try:
        venv = config['FMDT']['venv']
    except:
        venv = ''
    logx_params = {}
    logx_params['--log-path'] = pth_log
    logx_params['--trk-roi-path'] = detect_params['--trk-roi-path']
    logx_params['--fra-path'] = pth_log + 'frames.json'
    logx_params['--ftr-path'] = log_params['--ftr-path']
    logx_params['--trk-path'] = log_params['--trk-path']
    logx_params['--trk-bb-path'] = log_params['--trk-bb-path']
    for prm in ['--log-path','--trk-roi-path','--fra-path','--ftr-path','--trk-path','--trk-bb-path']:
        if not pth_out in logx_params[prm]:
            logx_params[prm] = pth_out + logx_params[prm]
    logx_params = {**log_params, **logx_params}
    cmd = fmdt_exe['log-parser'] + ' ' + ' '.join([f'{key} {value}' for key, value in logx_params.items()]) 
    log.info('* Launch log cmd: '+cmd)
    submitt_fmdt(cmd,crash=crash,nosub=nofmdt,venv=venv)
    
    
    # build fmdt-visu command
    vis_params = {}
    if visu_pth is not None:
        vis_params['--vid-in-path'] = visu_pth + root + ext
    else:
        vis_params['--vid-in-path'] = detect_params['--vid-in-path']
    vis_params['--trk-path'] = track_file
    vis_params['--trk-bb-path'] = logx_params['--trk-bb-path']
    vis_params['--vid-out-path'] = visu_file
    for prm in ['--trk-path','--trk-bb-path','--vid-out-path']:
        if not pth_out in vis_params[prm]:
            vis_params[prm] = pth_out + vis_params[prm]
    cmd  = fmdt_exe['visu'] + ' ' + ' '.join([f'{key} {value}' for key, value in vis_params.items()])
    log.info('* Launch visu cmd: '+cmd)
    submitt_fmdt(cmd,crash=crash,nosub=nofmdt)
    
    if not noXtract:
        # extract frames from original video for astrometry purpose
        # imgseq_track2frame(pth_in,track_file,'pgm',out_pth=pth_frame,fmt_o='%5d.png')
        imgseq_track2frame(pth_in,track_file,'pgm',out_pth=pth_frame,fmt_o='%5d.fits')
        # extract frames from visu video for visualisation purpose
        #video_track2frame(visu_file,track_file,out_pth=pth_frame,fmt='%5d.png')    
    
    # create fmdt_solve config file
    make_config_fmdt_solve(config_template,conf_file,pth_in,pth_out,pth_frame,
                            root,\
                            track_file,
                            logx_params['--trk-bb-path'],\
                            logx_params['--ftr-path'],\
                            logx_params['--fra-path'],\
                            detect_params['--log-path'],\
                            imgseq=True,
                            nosolve=nosolve)
    # launch fmdt-reduce
    log.info('Now launching fmdt_solve with config: '+conf_file+' '+track_file+' '+detect_params['--log-path'])
    fmdt_solve.fmdt_solve(conf_file)
    return

def make_config_fmdt_solve(config_template,conf_file,pth_in,pth_out,pth_frame,\
                            root,\
                            track_file,bb_file,mag_file,json_file,\
                            log_dir,\
                            imgseq=False,
                            nosolve=False,
                            fmt_ori='%5d.tiff'):
    """Creates a pyfmdt_solve configuration file.

    Parameters
    ----------
    config_template : string
        fmdt_solve template configuration file.
    config_file : string
        fmdt_solve output configuration file.
    pth_in : string
        Data directory.
    pth_out : string
        Output data directory.
    pth_frame : string
        Meteor frame file directory.
    root : string
        Data file root name.
    track_file : string
        FMDT output track file name.
    bb_file : string, optional
        FMDT box output file name.
    mag_file : string, optional
        FMDT magnitude output file name.
    json_file : string, optional
        FMDT json file concatenating all info.
    log_dir : string
        FMDT output log directory.
    imgseq : Boolean, optional
        If True, image sequence section is created. Default is False.
    fmt_ori : string, optional
        Original image format. Useful is imgseq is True. Default is '.tiff'.
    nosolve : Boolean, optional
        If True the solve-field command is not run. Useful for debug purpose. Default is False.
    
    Returns
    -------
    None.

    """  
    # debug
    log.debug('config_template: '+config_template)
    log.debug('conf_file: '+conf_file)
    log.debug('pth_in: '+pth_in)
    log.debug('track_file: '+track_file)
    log.debug('bb_file: '+bb_file)
    log.debug('mag_file: '+mag_file)
    log.debug('json_file: '+json_file)
    log.debug('log_dir: '+log_dir)
    # read template config file
    check_file(config_template)
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_template)
    config['USER']['home'] = os.getenv('HOME') + '/'
    config['USER']['data_dir'] = pth_in
    config['USER']['out_dir'] = pth_out
    config['USER']['frame_dir'] = pth_frame
    config['USER']['root'] = root.replace('__','_')
    config['USER']['submit'] = str(not nosolve)
    config['FMDT']['track_file'] = track_file
    config['FMDT']['log_dir'] = log_dir
    config['FMDT']['trk-meteor-min'] = str(detect_params['--trk-meteor-min'])
    config['FMDT']['bb_file'] = bb_file
    config['FMDT']['mag_file'] = mag_file
    config['FMDT']['json_file'] = json_file
    # set the image sequence section
    if imgseq:
        config['USER']['type'] = 'img_seq'
        # config['IMGSEQ']['pth_ori'] = pth_in.replace('_processed','')
        # config['IMGSEQ']['fmt_ori'] = fmt_ori
    else:
        config['USER']['type'] = 'video'
    # write changes back to file
    with open(conf_file,'w') as fo:
        config.write(fo)
    log.info('fmdt_solve configuration file saved in '+conf_file)
    return 


def create_dict_from_args(params):
    key_value_dict = {}
    for pair in params:
        key, value = pair.split('=')
        key_value_dict[key] = value
    return key_value_dict

if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='launch_fmdt.py arguments.')
    parser.add_argument('-i', default='./', help='Input data path. Default is: ./')
    parser.add_argument('-o', default='./', help='Output data path. Default is: ./')
    parser.add_argument('-p', default='', help='Video or Images file pattern. Example 1: p="Basler" if all video files starts with "Basler_". Example 2: for an image sequence stored in /my/path/, then p="image_" if pgm files are of pattern /my/path/image_*.pgm. Default is empty string.')
    parser.add_argument('-x', default='.mkv', help='Input file extension. Ex: .mkv  for video, or .pgm for image sequence. Default is .mkv')
    parser.add_argument('-img', action='store_true', help='If set, image sequence is considered, rather than video. Default is False.')
    parser.add_argument('-r', default='', help='FMDT output file root name. Default is empty string.')
    parser.add_argument('-c', default=home+'/fmdt/pyFMDT/conf/config_template.in', help='fmdt_solve configuration file template full path name. Default is: $home/fmdt/pyFMDT/conf/config_template.in')
    parser.add_argument('-fx', default=home+'/fmdt/build/exe/fmdt-detect-rt-pip', help='FMDT-detect full path executable file. Default is: $home/fmdt/build/exe/fmdt-detect-rt-pip')
    parser.add_argument('-fv', default=home+'/fmdt/build/exe/fmdt-visu', help='FMDT-visu full path executable. Default is: $home/fmdt/build/exe/fmdt-visu')
    parser.add_argument('--nofmdt', action='store_true', help='If set, FMDT is not launched. Useful for debug purpose.')
    parser.add_argument('--nosolve', action='store_true', help='If set, the solve-field script is not launched. Useful for debug purpose.')
    parser.add_argument('--noXtract', action='store_true', help='If set, frame extraction is not performed. Useful for debug purpose.')
    parser.add_argument('--params', nargs='+', help='fmdt executable parameters key-value pairs (e.g., key1=value1 key2=value2)')
    args = parser.parse_args()
    if args.params:
        exe_params = create_dict_from_args(args.params)
    else:
        exe_params = {}
    
    # store arguments
    pth_in = args.i
    pth_out = args.o
    fil_pat = args.p
    ext = args.x
    out_root = args.r
    config_template = args.c
    imgseq = args.img
    nofmdt = args.nofmdt
    nosolve = args.nosolve
    noXtract = args.noXtract
    
    # change ext if image sequence to comply with FMDT software.
    if imgseq:
        ext = 'pgm'
    
    # launch FMDT
    launch_fmdt(pth_in,pth_out,fil_pat,ext,out_root,config_template,
                exe_params=exe_params,
                imgseq=imgseq,nofmdt=nofmdt,nosolve=nosolve,
                noXtract=noXtract)
else:
    log.debug('successfully imported')

