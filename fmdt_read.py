#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 12:40:42 2022

@author: vaubaill
"""
# Read FMDT software output data files.
import re
import json
import numpy as np
import astropy.units as u
from astropy.table import QTable

from fmdt_log import log

def read_fmdt_outputs(track_file,bb_file,mag_file,json_file):
    """Read FMDT output data.

    Parameters
    ----------
    track_file : string
        FMDT tracking output data file name.
    bb_file : string
        FMDT bb output data file name.
    mag_file : string
        FMDT magnitude output data file name.
    json_file : string, optional
        FMDT json file concatenating all info.
        
    Returns
    -------
    trk_data : astropy.table.QTable object.
        FMDT tracking output data.
    bb_data : astropy.table.QTable object.
        FMDT bb output data.
    mag_data : astropy.table.QTable object.
        FMDT magnitude output data.

    """
    trk_data = read_track(track_file)
    bb_data = read_bb(bb_file)
    mag_data = read_mag(mag_file)
    json_data = read_json(json_file)
    return trk_data,bb_data,mag_data,json_data

def read_track(track_file):
    """Read FMDT output track data.

    Parameters
    ----------
    track_file : string
        FMDT tracking output data file name.


    Returns
    -------
    trk_data : astropy.table.QTable object.
        FMDT tracking output data.

    """
    log.debug('Now read: '+track_file)
    col_names = ['id','f_b','x_b','y_b','f_e','x_e','y_e','type']
    #trk_data = QTable().read(track_file,format='ascii.fixed_width',data_start=0,names=col_names)
    
    # List to hold the parsed data
    rows = []
    
    # Read the file and parse the data
    with open(track_file, 'r') as f:
        for line in f:
            # Skip comment lines starting with '#'
            if line.startswith('#'):
                continue
            
            # Remove leading and trailing whitespaces
            line = line.strip()
            
            # Use regex to split by '||' and remove any unwanted spaces
            columns = re.split(r'\s*\|\|\s*', line)
            
            if len(columns) == 4:  # Ensure it has the right number of columns
                track_id = int(columns[0])
                begin_frame, begin_x, begin_y = map(float, columns[1].split('|'))
                end_frame, end_x, end_y = map(float, columns[2].split('|'))
                obj_type = columns[3]
                
                # Append the row as a list of values
                rows.append([track_id, begin_frame, begin_x, begin_y, end_frame, end_x, end_y, obj_type])
    
    # Create an astropy QTable from the parsed data
    trk_data = QTable(rows=rows, names=col_names,dtype=['int','int','float','float','int','float','float','str'])
    
    # Assign units to the appropriate columns
    trk_data['x_b'].unit = u.pixel
    trk_data['y_b'].unit = u.pixel
    trk_data['x_e'].unit = u.pixel
    trk_data['y_e'].unit = u.pixel    
    
    return trk_data

def read_bb(bb_file):
    """Read FMDT output track data.

    Parameters
    ----------
    bb_file : string
        FMDT bb output data file name.


    Returns
    -------
    bb_data : astropy.table.QTable object.
        FMDT bb output data.

    """
    log.debug('Now read: '+bb_file)
    col_names = ['frame','Xr','Yr','Xc','Yc','id','sat']
    bb_data = QTable().read(bb_file,format='ascii.fast_no_header',names=col_names)
    return bb_data

def read_mag(mag_file):
    """Read magnitude data.

    Parameters
    ----------
    mag_file : string
        FMDT magnitude output data file name.
    
    Returns
    -------
    mag_data : dict
        FMDT magnitude output data.
        id : FMDT object id
            type : string
                FMDT object type: star, meteor, noise.
            data : numpy.array
                FMDT pixel count for each frame.
    
    """
    log.debug('Now read: '+mag_file)
    mag_data = {}
    mag_lines = read_mag_lines(mag_file)
    for line in mag_lines:
        data = line.split()
        objid = int(data[0])
        mag_data[objid] = {'type': data[1], 'data': np.array(data[2:],dtype='int')}
    return mag_data

def read_mag_lines(mag_file):
    """Read magnitude data.

    Parameters
    ----------
    mag_file : string
        FMDT magnitude output data file name.
    objid : int
        FMDT object id for which pixel counts are to be retrieved.

    Returns
    -------
    mag_data : astropy.table.QTable object.
        FMDT magnitude output data.

    """
    with open(mag_file,'r') as f:
        lines = f.readlines()
    return lines

def get_obj_mag(mag_lines,objid):
    """Read magnitude data.

     Parameters
    ----------
    mag_lines : string
        Lines in the FMDT magnitude data file.
    objid : int
        FMDT object id for which pixel counts are to be retrieved.

    Returns
    -------
    mag_data : astropy.table.QTable object.
        FMDT magnitude output data.

    """
    for line in mag_lines:
        if line.lstrip().startswith(str(objid)):
            mag_data = line.split()
    return mag_data


def read_json(json_file):
    """Read json data.
    
    Parameters
    ----------
    json_file : string
        json file name.
        
    Returns
    -------
    json_data
        Data in json format.
    
    """
    with open(json_file, 'r') as file:
        json_data = json.load(file)
    
    return json_data

def read_log_data(log_dir,frame_id,first_detect=False,trk_meteor_min=0):
    """Read FMDT log data for specific frame.
    
    Parameters
    ----------
    log_dir : string
        FMDT output log directory name.
    frame_id : int
        frame number to read data from.
    first_detect: boolean, opional
        True if this is the first frame where the meteor is identified. Default is False.
    trk_meteor_min : integer, optional
        FMDT number of frames to consider before identificating a meteor. Default is 0.
    
    """
    frame_idstr = f"{frame_id:05d}"
    filename = log_dir + frame_idstr + '.txt'
    log_data = read_tables_from_logfile(filename)
    
    return log_data

def parse_table(data_lines, columns):
    """ Parse the data lines of a table into a list of dictionaries."""
    table_data = []
    for line in data_lines:
        # Clean the line and split by whitespace
        clean_line = re.sub(r'[|]', ' ', line.strip())  # remove '|'
        split_line = clean_line.split()
        
        if len(split_line) != len(columns):
            log.debug('incomplete line detected: '+line)
            continue  # skip incomplete lines
        
        # Create a dictionary with column names as keys and row data as values
        row_data = {columns[i]: split_line[i] for i in range(len(columns))}
        table_data.append(row_data)
    
    return table_data

def create_qtable(table_data):
    """ Convert a list of dictionaries to an astropy QTable."""
    columns = {key: [] for key in table_data[0]}  # initialize column dictionary

    for row in table_data:
        for key, value in row.items():
            # Append the data to corresponding column, converting to floats if possible
            try:
                columns[key].append(float(value))
            except ValueError:
                columns[key].append(value)  # keep as string if it can't be converted
            except:
                columns[key].append(0.0)
    
    # Create a QTable from the columns
    qtable = QTable(columns)
    log.debug(str(qtable))
    
    return qtable

def line2nbroiNframe(line):
    """Get number of RoI from line
    
    """
    frame = line.split('n°')[1].split(' ')[0]
    nb_roi = int(line.split('[')[1].split(']')[0])
    return frame,nb_roi

def lines2begendtab(lines):
    """Get line numbers
    
    """
    line_id = -1
    for line in lines:
        line_id += 1
        if '(t-1)' in line:
            prev_b = line_id
        if '(t)' in line:
            prev_e = line_id - 1
            curr_b = line_id
        if 'Association' in line:
            curr_e = line_id - 1
            asso_b = line_id
        if 'Motion' in line:
            asso_e = line_id - 1
            moti_b = line_id
        if 'Tracks' in line:
            moti_e = line_id - 1
            trck_b = line_id
        trck_e = line_id
    log.debug(str([prev_b,prev_e,curr_b,curr_e,asso_b,asso_e,moti_b,moti_e,trck_b,trck_e]))
    return (prev_b,prev_e,curr_b,curr_e,asso_b,asso_e,moti_b,moti_e,trck_b,trck_e)

def read_subtab(lines,str4col,roi=None):
    """Read RoI lines
    
    lines : strings
        lines of text file.
    
    """
    frame,nb_roi = None,None
    columns = None
    table_data = []
    # loop over lines
    for line in lines:
        if roi and not frame:
            frame,nb_roi = line2nbroiNframe(line)
            continue
        # get names
        if str4col in line:
            #clean_line = re.sub(r'[|]', ' ', line.strip()).replace('#','').replace('||','|')  # remove '|'
            clean_line = line.replace('#','').replace('||','|').strip().replace(' ','')
            columns = clean_line.split('|')
            # correct columns names if needed
            if roi:
                columns[0] = 'roi_ID'
                columns[1] =  'trk_ID'
                columns[15] = 'Flux'
            continue
        # loop if comment
        if line.startswith('#'):
            continue
        # otherwise stopre data in table_data for later parse
        if columns:
            # We're inside a table, collect the data rows
            table_data.append(line)

    parsed_data = parse_table(table_data, columns)
    data = create_qtable(parsed_data)
    
    return (frame,nb_roi,data)

def read_tables_from_logfile(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    (prev_b,prev_e,
     curr_b,curr_e,
     asso_b,asso_e,
     moti_b,moti_e,
     trck_b,trck_e) = lines2begendtab(lines)
    
    (frame_prev,nb_roi_prev,data_prev) = read_subtab(lines[prev_b:prev_e],'ID',roi='(RoI)')
    (frame_curr,nb_roi_curr,data_curr) = read_subtab(lines[curr_b:curr_e],'ID',roi='(RoI)')
    (frame_asso,nb_roi_asso,data_asso) = read_subtab(lines[asso_b:asso_e],'t-1')
    #(frame_moti,nb_roi_moti,data_moti) = read_subtab(lines[moti_b:moti_e],'theta')
    #(frame_trck,nb_roi_trck,data_trck) = read_subtab(lines[trck_b:trck_e],'State')
    
    all_data = {'prev':data_prev,
                'curr':data_curr,
                'asso':data_asso} # 'moti':data_moti,'trck':data_trck}
    
    return all_data


