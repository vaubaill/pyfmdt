#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  3 09:35:01 2023

@author: vaubaill
"""
import logging
from astropy.time import Time

level = logging.DEBUG

# logging
log = logging.getLogger(__name__)
log.setLevel(level)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '

# add stream handler
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.propagate = True
log.addHandler(sdlr)

# add file handler for warning and error messages
# lof = logging.getLogger(__name__)
# lof.setLevel(level)
log_file = 'fmdt_' + Time.now().isot.replace('-','').replace(':','').split('.')[0] + '.log' 
fdlr = logging.FileHandler(log_file)
fdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(fdlr)
log.propagate = True

# lof.addHandler(fdlr)
# lof.propagate = True
