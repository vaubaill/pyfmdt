#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 09:21:59 2025

@author: vaubaill
"""

from PIL import Image
import numpy as np


def gamma_correction(image_array, gamma=1.0):
    """
    Apply gamma correction to the image array.
    
    :param image_array: numpy array of the image
    :param gamma: gamma value, typically in the range (0.1 - 5.0), 1.0 means no change
    :return: gamma-corrected image array
    """
    # Normalize the image array to [0, 1]
    norm_image = image_array / 255.0
    
    # Apply gamma correction
    corrected_image = np.power(norm_image, gamma)
    
    # Scale back to [0, 255]
    corrected_image = np.clip(corrected_image * 255, 0, 255).astype(np.uint8)
    
    return corrected_image

def image_difference_with_gamma(image1_path, image2_path, output_path="difference_image.png", gamma=1.0):
    """
    Calculate the difference between two images and apply gamma correction.
    
    :param image1_path: path to the first image
    :param image2_path: path to the second image
    :param output_path: where to save the result
    :param gamma: gamma value for contrast adjustment
    """
    # Open the two images
    image1 = Image.open(image1_path)
    image2 = Image.open(image2_path)
    
    # Ensure the images are the same size and mode
    if image1.size != image2.size or image1.mode != image2.mode:
        raise ValueError("Images must have the same size and mode.")
    
    # Convert the images to numpy arrays
    img1_array = np.array(image1, dtype=np.float32)
    img2_array = np.array(image2, dtype=np.float32)
    
    # Calculate the absolute difference between the two images
    diff_array = np.abs(img1_array - img2_array)
    
    # Apply gamma correction to the difference image
    diff_array = gamma_correction(diff_array, gamma)
    
    # Convert the numpy array back to a PIL image
    diff_image = Image.fromarray(diff_array)
    
    # Save the resulting difference image
    diff_image.save(output_path)
    print(f"Difference image with gamma correction saved to {output_path}")

# Example usage:
image1_path = "/home/vaubaill/PROJECTS/PODET/PODET-MET/pyfmdt/20241212T203633/average_image.png"
image2_path = "/home/vaubaill/PROJECTS/PODET/PODET-MET/pyfmdt/20241212T203633/diff/average_image.png"
output_path = "/home/vaubaill/PROJECTS/PODET/PODET-MET/pyfmdt/20241212T203633/20241212T203633_difference_image.png"
gamma_value = 0.3  # Increase gamma to brighten the image and highlight subtle differences

image_difference_with_gamma(image1_path, image2_path, output_path, gamma=gamma_value)


