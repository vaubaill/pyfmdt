#!/usr/bin/bash

# TODO: description du script

# User settings
#subdir="Basler_2040"
#newdir="Basler_acA2040-55um__23303281__20220531_034613537"
subdir=$1
pattern=$2

# verifications
if (test -z $subdir) # prevents null string
then
        echo "**** Fatal error: plz specify subdir"
        echo "**** syntax is: ctodo.sh subdir pattern"
        exit
fi
if (test -z $pattern) # prevents null string
then
        echo "**** Fatal error: plz specify pattern"
        echo "**** syntax is: ctodo.sh subdir pattern"
        exit
fi



# program main body
echo "Making new directory $pattern"
!(test -d $pattern) && mkdir $pattern
cd $pattern 
pwd
cmd="mv ../$pattern*.tiff ./"
echo "Moving files with the cmd: $cmd"
mv ../$pattern\_00*.tiff ./
mv ../$pattern\_01*.tiff ./
mv ../$pattern\_02*.tiff ./
mv ../$pattern\_03*.tiff ./
mv ../$pattern\_04*.tiff ./
mv ../$pattern\_05*.tiff ./
mv ../$pattern\_06*.tiff ./
mv ../$pattern\_07*.tiff ./
mv ../$pattern\_08*.tiff ./
mv ../$pattern\_09*.tiff ./
mv ../$pattern\_10*.tiff ./
mv ../$pattern\_11*.tiff ./
mv ../$pattern\_12*.tiff ./
mv ../$pattern\_13*.tiff ./
cmd="pwd ; ffmpeg -i $pattern\_%04d.tiff -framerate 20 -c:v libx264 -crf 0 -pix_fmt yuv420p -preset veryslow ../$pattern.mkv"
echo "cmd=$cmd"
echo "Making video file (please wait...)"
#pwd ; ffmpeg -i $pattern\_%04d.tiff -framerate 20 -c:v libx264 -crf 0 -pix_fmt yuv420p -preset veryslow ../$pattern.mkv
cd ..
echo "video $pattern.mkv done."



# maybe todo:
#newdir="Basler_acA1920-155um__23278758__20220531_034611281"
#ffmpeg -i $pattern_%05d.tiff -vf fps=20 $pattern-2.mp4
