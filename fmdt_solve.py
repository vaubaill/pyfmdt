#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 14:17:42 2022

@author: vaubaill
"""
# modify RMS .config file and put correct plane position and heading.

import os
import re
import glob
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import astropy.units as u
from astropy.io import fits
from astropy.wcs import WCS
from astropy.time import Time,TimeDelta
from astropy.table import QTable
from astropy.coordinates import SkyCoord,match_coordinates_sky
from astroquery.gaia import Gaia
from sklearn.linear_model import RANSACRegressor
from configparser import ConfigParser, ExtendedInterpolation
from photutils.aperture import CircularAnnulus, CircularAperture,ApertureStats
from photutils.aperture import aperture_photometry


import fmdt_read
from fmdt_log import log

# global variable init
config = None

def root2time(root,yyyymmdd=''):
    """Get start of observation from directory name or file name.
    
    Parameters
    ----------
    root : string
        Directory or image file name.
    yyyymmdd : string, optional
        Date. Useful for Fabs data that provide only the time.
        Output time is YYYY-MM-DDTHH:MM:SS.ssss
    
    Returns
    -------
    time_start : astropy.time.Time Object
        time of start of observation
    
    Example:
        -Name of image file is: Basler_acA1920-155um__23278758__20220531_042259204_00012
        => time = 2022-05-31T04:22:59.204
        -Name of video is W3_04_55_40 and yyyymmdd='2022-05-31' 
        => output time is 2022-05-31T04:55:40
    
    """
    # Case of type 'W3_04_55_40' (Fabs)
    if yyyymmdd:
        tmstr = ':'.join(root.split('_')[1:])
        time_start = Time(yyyymmdd+'T'+tmstr)
    
    # case of type Basler_acA1920-155um__23278758__20220531_042259204_00012.pgm
    else:
        tmstr1 = root.split('_')[-3:-1]
        tmstr2 = tmstr1[0][:4]+'-'+tmstr1[0][4:6]+'-'+tmstr1[0][6:8] + 'T' + \
                 tmstr1[1][0:2] + ':' + tmstr1[1][2:4] + ':' + tmstr1[1][4:6] + \
                 '.' + tmstr1[1][6:]
        time_start = Time(tmstr2)
        
    return time_start

def fits2dat(fitsfile,sort=''):
    """Convert a fits file into a dat file.
    
    Parameters
    ----------
    fitsfile : string
        Input fits file.
    sort : string, optional
        If present, data are sorted according the the colun name sort.
    
    Returns
    -------
    None. The output file has the same name as input file,
        with extension renamed ".dat".
    
    """
    datfile = fitsfile.replace('.fits','.dat')
    hdu = fits.open(fitsfile)
    data = QTable(hdu[1].data)
    if sort:
        data.sort(sort)
    # set format if needed
    try: 
        data['field_x'].info.format = '7.2f'
        data['field_y'].info.format = '7.2f'
        data['index_x'].info.format = '7.2f'
        data['index_y'].info.format = '7.2f'
        data['field_ra'].info.format = '7.3f'
        data['field_dec'].info.format = '5.2f'
        data['index_ra'].info.format = '7.3f'
        data['index_dec'].info.format = '5.2f'
        data['match_weight'].info.format = '7.5f'
        data['FLUX'].info.format = '7.2f'
        data['BACKGROUND'].info.format = '5.2f'
    except:
        pass
    
    data.write(datfile,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('Data saved in '+datfile)
    
    return

def read_config(config_file):
    """

    Parameters
    ----------
    config_file : String
        pyFMDT configuration file.

    Returns
    -------
    config : configparser.ConfigParser object.
        FMDT configuration.
    img_sz : list of 2 astropy.units.Quantity objects
        image size: [width,height] in [pix].
    fov_sz : list of 2 astropy.units.Quantity objects
        Size of field of view: [width,height] in [deg].
    img_scale : astropy.units.Quantity objects
        Image scale in [deg/pix].
    fps : astropy.units.Quantity objects
        Camera frame per second, in [1/s]
    time_start : astropy.time.Time object.
        Video sequence start time.
    LimMag : astropy.units.Quantity objects
        Camera estimated limiting magnitude.
    band : string, optional.
        Band of data acquisition. Choice is: 'V', 'B', 'R'.
        Default is 'V'.
    avgflx : int
        Number of frames to average to compute the star image flux.
        Deafult is 0, i.e. no averaging.
    match_tol_fct : float
        Star matching tolerance factor, in [pixel]. Star matching is performed
        at a maximum distance of match_tol_fct pixels. Default is 1.0.
    read_astrometry : boolean
        If True, astrometry results are red instead of computed. Useful for
        debug purpose to speed up the whole process. False is not absent from
        the configuration file.
    read_photometry : boolean
        fI True, photometry results are red instead of computed. Useful for
        debug purpose to speed up the whole process. False is not absent from
        the configuration file.
    opth : string
        Output directory.
    fpth : string
        Meteor frame directory.
    fmt : string
        Meteor frame format.
    root : string
        Output root file name.
    log_dir : string
        FMDT output log directory.
    trk_meteor_min : integer
        FMDT trk-meteor-min (minimum number of frames to consider to detect a meteor) value
    submit : boolean
        If False, the solve-field command is not performed.
        
    """
    global config
    # check existence of configuration file
    if not os.path.exists(config_file):
        msg = '*** FATAL ERROR: configuration file does not exist: '+config_file
        log.error(msg)
    # read configuration file
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_file)
    config['USER']['home'] = os.getenv('HOME') + '/'
    # change log level
    logdict = {'DEBUG' : logging.DEBUG,
               'INFO' : logging.INFO,
               'WARNING' : logging.WARNING,
               'ERROR' : logging.ERROR}
    log.setLevel(logdict[config['USER']['log_level']])
    log.propagate = True
    opth = config['USER']['out_dir']+'/'
    root = config['USER']['root']
    fpth = config['USER']['frame_dir']
    if not fpth.endswith('/'):
        fpth=fpth+'/'
    try:
        fmt = config['USER']['fmt']
    except KeyError:
        fmt = '%5d.pgm'
    
    # read CAMERA section
    img_width = int(config['CAMERA']['image_width'])*u.pix
    img_height = int(config['CAMERA']['image_height'])*u.pix
    fov_width = float(config['CAMERA']['fov_width'])*u.deg
    fov_height = float(config['CAMERA']['fov_height'])*u.deg
    # estimate scale
    img_scale = fov_width / img_width
    fps = float(config['CAMERA']['fps']) / u.s
    if 'auto' in config['CAMERA']['time_start']:
        time_start = path2time(config['USER']['data_dir'])
    else:
        time_start = Time(config['CAMERA']['time_start'],scale='utc')
    img_sz = [img_width,img_height]
    fov_sz = [fov_width, fov_height]
    LimMag = float(config['CAMERA']['LM']) * u.mag
    band = config['CAMERA']['band']
    
    # create output directory if needed
    if not os.path.exists(opth):
        log.info('Make output directory '+opth)
        os.mkdir(opth)
    
    # read PROCESS section
    try:
        avgflx = int(config['PROCESS']['avgflx'])
    except KeyError:
        avgflx = 0
    # set read_res parameter: useful to read results if the simulation has already been performed.
    try:
        read_astrometry = config['PROCESS']['read_astrometry'].lower() in ['true', '1', 't', 'y', 'yes']
    except:
        read_astrometry = False
    try:
        read_photometry = config['PROCESS']['read_photometry'].lower() in ['true', '1', 't', 'y', 'yes']
    except:
        read_photometry = False
    match_tol_fct = float(config['PROCESS']['match_tol_fct'])
    
    # set the submit boolean
    try:
        submit = config['USER']['submit'].lower() in ['true', '1', 't', 'y', 'yes']
    except:
        submit = True    
    
    # read FMDT section
    log_dir = config['FMDT']['log_dir']
    trk_meteor_min = int(config['FMDT']['trk-meteor-min'])
    return (config,img_sz,fov_sz,img_scale,fps,time_start,LimMag,band,\
            avgflx,match_tol_fct,read_astrometry,read_photometry,opth,fpth,fmt,
            root,log_dir,trk_meteor_min,submit)


def path2time(pth):
    """Get acquisition start time from path name.
    
    Parameters
    ----------
    pth : string
        Path name.

    Raises
    ------
    ValueError
        Path name cannot be converted into time.

    Returns
    -------
    time : astropy.time.Time Object
        Time of start of acquisition.

    """
    # get end of path
    try:
        match = re.search(r'(\d{8}_\d{9})', pth)
        timestamp_str = match.group(1)
        # Split the date and time parts
        date_str, time_str = timestamp_str.split('_')
        # Format the timestamp into a proper datetime string (YYYY-MM-DDTHH:MM:SS.sss)
        formatted_timestamp = f"{date_str[:4]}-{date_str[4:6]}-{date_str[6:8]}T{time_str[:2]}:{time_str[2:4]}:{time_str[4:6]}.{time_str[6:]}"        
        # Convert to astropy.time.Time object
        time = Time(formatted_timestamp)
        [(ddate,ttime)] = re.findall('.*(\d{8})_(\d{9}).*',pth)
    except:
        msg = '*** FATAL ERROR: impossible to get date and time from string: '+pth
        log.error(msg)
        raise ValueError(msg)
    return time

def frame2filename(pth_frame,root,frame,fmt='%5d.pgm',exclude='_visu',met_id=None,opth=None):
    """Retrieve frame image file from frame number.

    Parameters
    ----------
    pth_frame : string
        Frame directory name.
    root : string
        Root file name.
    frame : int
        Frame number.
    fmt : string, optional
        File extenstion. Default is '%5d.pgm'.
    met_id : integer, optional
        If set, meteor id. Default is None. Useful to retrieve meteor frame file.
    exclude : string, optional
        File name excluding pattern. Default is '_visu'.
    opth : string, optional
        Output path, if different from theinput path. Default is None.
    
    Returns
    -------
    frame_file : string
        Frame file full path name.
    
    """
    for p in [pth_frame,opth]:
        if not p.endswith('/'):
            p = p + '/'
    # extract root, extension and number of digits in name extension
    [f_fmt,f_ext] = fmt.split('.')
    dig = int(f_fmt.replace('%','').replace('d',''))
    frmstr = '_'+str(frame).zfill(dig)
    pattern = pth_frame+root+'*'+frmstr+'.'+f_ext
    log.debug('pth_frame: '+pth_frame)
    log.debug('root: '+root)
    log.debug('frmstr: '+frmstr)
    log.debug('f_ext: '+f_ext)
    log.debug('Get list of images of pattern: '+pattern+' excluding: '+exclude)
    list_img = glob.glob(pattern)
    list_img = [i for i in list_img if exclude not in i]
    # check for multiple file
    if len(list_img)>1:
        metstr = str(met_id).zfill(dig) if met_id else ''   
        pattern = pth_frame+'*'+metstr+'*'+frmstr+'.'+f_ext
        list_img = glob.glob(pattern)
        list_img = [i for i in list_img if exclude not in i]
        # if there are still more than 1 file that matches the pattern
        if len(list_img)>1:
            msg = '*** FATAL ERROR: more than one file matches '+pattern+' : '+str(list_img)
            log.error(msg)
            raise IOError(msg)
    frame_file = list_img[0]
    if opth:
        frame_file = opth + os.path.basename(frame_file)
    # remove '//'
    frame_file.replace('/','')
    log.debug('frame file: '+frame_file)
    return frame_file

def match_stars(astrometry_data,wcs_obj,radec_center,fov_sz,maxsep, \
                frame=1,LimMag=6*u.mag,opth='./',root='',nmatch_min=10):
    """Match image stars with catalog stars.
    
    Output data are saved in opth directory. File names start with root.

    Parameters
    ----------
    astrometry_data : astropy.table.Qtable object.
        Astrometry data QTable.
    wcs_obj : astropy.wcs.WCS object.
        World coordinate system.
    radec_center : astropy.coordinate.SkyCoord object.
        Sky coordinates of center of field of view.
    fov_sz : list of 2 astropy.units.Quantity objects.
        Field of view: width and height.
    maxsep : astropy.units.Quantity object
        Maximum angular separation for star matching.
    frame : int, optional
        Frame number. Default is 1.
    LimMag : astropy.units.Quantity object, optional.
        Camera limiting magnitude. Default is 6.0*u.mag.
    opth : string, optional
        Output directory name. Default is './'.
    root : string, optional
        Output file root name. Default is ''.  
    cat : string, optional
        Catalog Vizier name. Default is 'II/355/ugriz-2', corresponding to 
        A catalog of Sloan magnitudes for the brightest stars - version 2.
        Catalog limiting magnitude=7, 3969 stars with photometry.
        'J/PASP/120/1128': Calibrated griz magnitudes of Tycho stars (Ofek, 2008)
        Catalog of griz magnitudes for Tycho-2 stars with BT<13mag and VT<12mag (1560980 rows)
    nmatch_min : int, optional
        Mnimal number of matched stars in order to perfor photmetry calibration.
        Default is 10.
    
    See also
    --------
    GAIA or Hipparcos catalog query: 
       https://astroquery.readthedocs.io/en/latest/gaia/gaia.html
    
    Returns
    -------
    matched_star : astropy.table.Qtable object.
        Catalog star coordinates and V-magnitude.

    """
    # output file name
    if not root.endswith('_'):
        root = root + '_'
    framestr = f"{frame:05d}"
    cat_file = opth+root+'star_catalog_'+framestr+'.dat'
    matched_file = opth+root+'star_matched_'+framestr+'.dat'
    # get FOV 
    [fov_width,fov_height] = fov_sz
    fov = np.sqrt(fov_width**2 + fov_height**2) / 2.0
    # transform star coorindates into sky coordinates
    radec_star = SkyCoord(ra=astrometry_data['RA'],
                          dec=astrometry_data['DEC'],
                          frame="icrs")
    log.debug('star RADEC: '+str(radec_star))
    log.debug('LimMag: '+str(LimMag))
    # define query limits
    # buld the query 
    # query_cmd = "SELECT TOP 100 " + \
    #             "ra, dec, phot_g_mean_mag, " + \
    #             "phot_bp_mean_mag, phot_rp_mean_mag " + \
    #             "FROM gaiadr3.gaia_source AS gaia " + \
    #             "WHERE " + \
    #             "gaia.ra BETWEEN "+str(ra_min.to('deg').value)+" AND "+str(ra_max.to('deg').value)+"  AND " + \
    #             "gaia.dec BETWEEN "+str(de_min.to('deg').value)+"  AND "+str(de_max.to('deg').value)+"  AND " + \
    #             "phot_g_mean_mag<="+str(LimMag.value)
    query_cmd = "SELECT TOP 200 " + \
                "ra, de, vmag " + \
                "FROM public.hipparcos AS hip " + \
                "WHERE " + \
                'DISTANCE('+f"{radec_center.ra.to('deg').value:.1f}" + ',' + \
                            f"{radec_center.dec.to('deg').value:.1f}"+ ',' + \
                            'ra, de) < ' + f"{fov.to('deg').value:.1f} AND " + \
                " hip.vmag<="+str(LimMag.value) + \
                ' ORDER BY vmag'
    
    log.info('Catalog query_cmd: '+query_cmd)
    # query GAIA star catalog
    job = Gaia.launch_job(query_cmd)
    data_cat = job.get_results()
    data_cat.sort('vmag')
    try:
        data_cat.rename_column('de','dec')
        data_cat.rename_column('vmag','Vmag')
    except:
        pass
    # # estimate GAIA V-band magnitude
    # data_cat.rename_column('phot_g_mean_mag','Gmag')
    # data_cat.rename_column('phot_bp_mean_mag','Bmag')
    # data_cat.rename_column('phot_rp_mean_mag','Rmag')
    # bp_rp = data_cat['Bmag'] - data_cat['Rmag']
    # v_minus_g = 0.004 * (bp_rp**2) - 0.031 * bp_rp + 0.070
    # data_cat['Vmag'] = data_cat['Gmag'] + v_minus_g
    data_cat['ra'].info.format = '7.3f'
    data_cat['dec'].info.format = '5.3f'
    log.debug('data_cat: '+str(data_cat))
    data_cat.write(cat_file,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('catalog data saved in '+cat_file)
    
    # convert data_cat into Sky Coordinates
    radec_cat = SkyCoord(data_cat['ra'],data_cat['dec'],frame="icrs")
    # match star catalog to source catalog.
    idx, d2d, d3d = match_coordinates_sky(radec_star,radec_cat)
    log.debug('idx: '+str(idx))
    # add magnitude data in star data
    astrometry_data.add_column(radec_star.ra,name='RAimg')
    astrometry_data.add_column(radec_star.dec,name='DECimg')
    astrometry_data.add_column(data_cat['ra'][idx],name='RAcat')
    astrometry_data.add_column(data_cat['dec'][idx],name='DEcat')
    # astrometry_data.add_column(data_cat['Gmag'][idx])
    # astrometry_data.add_column(data_cat['Bmag'][idx])
    # astrometry_data.add_column(data_cat['Rmag'][idx])
    astrometry_data.add_column(data_cat['Vmag'][idx])
    # set format
    astrometry_data['RAimg'].info.format = '7.3f'
    astrometry_data['DECimg'].info.format = '5.3f'
    astrometry_data['RAcat'].info.format = '7.3f'
    astrometry_data['DEcat'].info.format = '5.3f'
    astrometry_data['Vmag'].info.format = '5.2f'
    astrometry_data.sort('Vmag')
    #astrometry_data.sort('RAimg')
    # proceed to the match
    log.info('Star matching criterion: '+str(maxsep))
    mask = d2d < maxsep
    matched_star = astrometry_data[mask]
    # remove stars for which the flux is 0
    mask = matched_star['Flux']>0
    matched_star = matched_star[mask]
    matched_star.sort('Flux',reverse=True)
    matched_star.write(matched_file,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('matched data saved in '+matched_file)
    nmatch = len(matched_star)
    if nmatch<nmatch_min:
        msg = '*** FATAL ERROR: only  '+str(nmatch)+' matched stars, i.e. less than '+str(nmatch_min)
        log.error(msg)
        #raise ValueError(msg)
    else:
        log.info('There are '+str(nmatch)+' matching stars')
    return matched_star




def make_photometry_fit(matched_data,met_flux,met_id=None,\
                        band='G',frame=1,opth='./',root='',\
                        method='RANSAC'):
    """Make photoetry fit and compute meteor magnitude.
    
    Data and plot are saved in opth directory.
    
    Parameters
    ----------
    matched_data : astropy.table.QTable
        Matched star data, including magnitude data.
    met_flux : float
        Meteor flux in [ADU].
    met_id : int, optional
        Meteor id. Default is None.
    band : string, optional
        Wavelength band. Choice is: 'V': visible, 'B': blue, 'R': red.
        Default is 'V'.
    frame : int, optional
        Frame number. Default is 1.
    opth : string, optional
        Output directory name. Default is './'.
    root : string, optional
        Output file root name. Default is ''.
    method : string, optional
        Method to perform the fit. Options are 'RANSAC' (sklearn.linear_model.RANSAC)
        or 'POLY' (numpy.polyfit). Defailt is 'RANSAC'.
    
    See also
    --------
    Band definition depends on requested star catalog. In fmdt-reduce we use
        the Hiparcos catalog.


    Returns
    -------
    met_mag : astropy.units.QTable
        Meteor magnitude.

    """
    # build band name vs star catalog column name
    b2c = {'V':'Vmag',
           'G':'Gmag',
           'B':'Bmag',
           'R':'Rmag'}
    mag_col = b2c[band]
    
    # sort data
    matched_data.sort('Flux',reverse=True)
    log_flx = np.log10(met_flux.value)
    # fit mag=f(flux)
    if 'POLY' in method.upper():
        # fit mag=log10(fit) from matched star data
        coefficients, cov_matrix = np.polyfit(np.log10(matched_data['Flux'].value),
                                              matched_data[mag_col].value,
                                              1,
                                              cov=True)  # returns [slope, intercept]
        errors = np.sqrt(np.diag(cov_matrix)) 
        # Create a fit function using the coefficients
        fit_line = np.poly1d(coefficients)
        # estimage meteor magnitude
        met_mag = fit_line(log_flx) * u.mag
        # compute meteor magnitude error
        jacobian_met = np.array([1, log_flx])
        # Compute the error for y_new using error propagation
        met_mag_err = np.sqrt(np.dot(jacobian_met, np.dot(cov_matrix, jacobian_met.T))) 
        # make fit data
        x_fit = np.linspace(min(np.log10(matched_data['Flux'].value)), max(np.log10(matched_data['Flux'].value)), 100)
        y_fit = fit_line(x_fit)
        jacobian = np.array([np.ones_like(x_fit), x_fit]).T
        y_err = np.sqrt(np.sum((jacobian @ cov_matrix) * jacobian, axis=1))     
    else:
        # fit mag=log10(fit) from matched star data
        ransac = RANSACRegressor(random_state=42)
        X_star = np.array(np.log10(matched_data['Flux'].value)).reshape(-1, 1)
        Y_star = np.array(matched_data[mag_col].value)
        ransac.fit(X_star,Y_star)
        # get fit coefficients
        slope = ransac.estimator_.coef_[0]
        intcp = ransac.estimator_.intercept_    
        log.debug('slope: '+str(slope))
        log.debug('intcp: '+str(intcp))
        # get inliers and outliers
        inlier_mask = ransac.inlier_mask_
        outlier_mask = np.logical_not(inlier_mask)
        inliers_X = X_star[inlier_mask]
        inliers_Y = Y_star[inlier_mask]
        outliers_X = X_star[outlier_mask]
        outliers_Y = Y_star[outlier_mask]
        # Calculate residuals (difference between actual and predicted Y values)
        residuals = Y_star - ransac.predict(X_star)
        # Estimate standard deviation of residuals (uncertainty in fit)
        sigma = np.std(residuals)
        # Compute the covariance matrix for the fitted parameters (slope and intercept)
        # This is done by computing the variance-covariance matrix for the regression parameters
        # Using formulae for the uncertainty of linear regression parameters
        X_ = np.c_[np.ones(X_star.shape[0]), X_star]  # Add a column of ones for the intercept term
        X_transpose = X_.T
        cov_matrix = np.linalg.inv(X_transpose @ X_) * (sigma ** 2)
        # The error on the fitted slope and intercept
        slope_error = np.sqrt(cov_matrix[1, 1])  # Error on the slope
        intcp_error = np.sqrt(cov_matrix[0, 0])  # Error on the intercept
        # For error bars on meteor magnitude predictions:
        # We need to calculate the uncertainty in the prediction for the meteor's flux
        # The uncertainty in the meteor magnitude can be computed using the standard deviation (sigma)
        # of residuals and the variance-covariance matrix for the fitted parameters.
        
        # Propagate the uncertainty for a single prediction
        def compute_prediction_error(flux):
            # For a given flux, compute the uncertainty of the prediction
            # First, calculate the Jacobian matrix (derivatives of prediction with respect to model parameters)
            jacobian = np.array([1, flux])  # Derivative of the model w.r.t slope and intercept
        
            # Use the formula for propagation of uncertainty:
            # sigma_pred = sqrt(Jacobian.T @ cov_matrix @ Jacobian)
            prediction_error = np.sqrt(jacobian.T @ cov_matrix @ jacobian) * sigma
            return prediction_error
        
        # compute meteor magnitude and error
        met_mag = ransac.predict(np.array(np.log10(met_flux.value)).reshape(-1, 1))[0] * u.mag
        met_mag_err = compute_prediction_error(np.log10(met_flux.value))* u.mag
        # create fitted data set
        x_fit = np.linspace(min(np.log10(matched_data['Flux'].value)), max(np.log10(matched_data['Flux'].value)), 100)
        y_fit = ransac.predict(np.array(x_fit).reshape(-1, 1))
        y_err = np.array([compute_prediction_error(flux) for flux in x_fit])
        
    
    # log meteor magnitude
    log.info('met_mag = '+str(met_mag)+' +- '+str(met_mag_err))
    
    # plot flux and mag
    if not root.endswith('_'):
        root = root + '_'
    if met_id:
        idstr = f"{met_id:03d}" + '_'
    else:
        idstr = ''
    plotfile = opth+root+'photfit_'+idstr+str(frame)+'.png'
    fig=plt.figure()
    # plot stars data
    #plt.scatter(X_star,Y_star,color='red',s=3,marker='P',label='Stars')
    plt.scatter(inliers_X,inliers_Y,color='red',s=3,marker='P',label='Stars (inliers)')
    plt.scatter(outliers_X,outliers_Y,color='blue',s=3,marker='P',label='Stars (outliers)')
    # plot fitted data
    plt.plot(x_fit, y_fit, color='blue', linestyle=':', label=f'Fit: slope = {slope:.2f} ± {slope_error:.2f}, intercept = {intcp:.2f} ± {intcp_error:.2f}')
    #plt.scatter(x_fit, y_fit + y_err/2.0, color='blue', label="Fit upper bound", marker='.', s=1)
    #plt.scatter(x_fit, y_fit - y_err/2.0, color='blue', label="Fit lower bound", marker='.', s=1)
    # plot meteor data
    plt.scatter(log_flx,met_mag.value,color='magenta',marker='*', s=50,label='Meteor')
    plt.errorbar(log_flx,met_mag.value, yerr=met_mag_err.value, fmt='.', color='magenta', markersize=1)
    plt.title('Flux vs Mag',color='indianred')
    plt.xlabel('log10(Flux)')
    plt.ylabel('mag')
    plt.gca().invert_yaxis()
    plt.legend(loc='upper right',fontsize=6)
    plt.savefig(plotfile,dpi=300)
    plt.close(fig)
    
    log.info('plot saved in '+plotfile)
    log.info('met mag: '+str(met_mag))
    return met_mag, met_mag_err


def save_n_plot_meteor(met_data,met_id,opth='./',root='',LimMag=6.0*u.mag):
    """Save and plot meteor data.

    Parameters
    ----------
    met_data : astropy.table.QTable object.
        Meteor data: time, frame, X, Y, RA, DEC, Flux, Mag.
    met_id : int
        FMDT track meteor id.
    opth : string, optional
        Output directory name. Default is './'.
    root : string, optional
        Output file root name. Default is ''.
    LimMag : astropy.units.Quantity object.
        Camera limiting magnitude. Default is 6.0*u.mag.
    
    Returns
    -------
    None.

    """
    if not root.endswith('_'):
        root = root + '_'
    idstr = f"{met_id:03d}"
    met_file = opth+'/'+root+'meteor_'+idstr+'.dat'
    met_data.write(met_file,format='ascii.fixed_width_two_line',
                   formats={'Time':'%23s','RA':'8.4f','DEC':'8.4f','Mag':'6.2f','Mag_err':'6.2f'},
                   overwrite=True)
    log.info('Meteor data saved in '+met_file)
    # plot meteor light curve
    # time_rel = met_data['Time'][0]-met_data['Time']
    # time_rel.info.unit = u.day
    # log.debug('time_rel: '+str(time_rel))
    # time_rel = time_rel.to('s')
    fig=plt.figure()
    #plt.plot(time_rel.to('s').value,met_data['Mag'].value,'bo')
    #plt.plot(met_data['frame'],met_data['Mag'].value,'bo')
    plt.errorbar(met_data['frame'],met_data['Mag'].value, yerr=met_data['Mag_err'].value, fmt='.', color='blue')
    plt.title(str(met_id)+' Meteor light curve',color='indianred')
    plt.xlabel('Frame #')
    plt.ylabel('Mag')
    plt.ylim(bottom=None, top=LimMag.value),
    plt.gca().invert_yaxis()
    plt.legend(loc='lower right')
    outfile = opth+root+'meteor_'+idstr+'_LC.png'
    plt.savefig(outfile,dpi=300)
    plt.close(fig)
    log.info('plot saved in '+outfile)
    return

def get_fov_from_solve(solve_out):
    """Get FOV from solve-field output text.
    
    Parameters
    ----------
    solve_out : string
        solve-field output text file.
    
    Returns
    -------
    fov_width : astropy.units.Quantity object.
        Width of field of view, in [deg].
    fov_height : astropy.units.Quantity
        Height of field of view, in [deg].
    
    """
    # opten solve-field output text file
    with open(solve_out) as f:
        lines = f.readlines()
        lines = ' '.join(lines)
    # search for RA/DEC center
    cntsrch = re.search(r"Field center: \(RA,Dec\) = \(([-+]?\d*\.\d+), ([-+]?\d*\.\d+)\) deg\.", lines)
    if cntsrch:
        ra_center = float(cntsrch.group(1)) * u.deg
        de_center = float(cntsrch.group(2)) * u.deg
        radec_center = SkyCoord(ra=ra_center,
                                dec=de_center,
                                frame="icrs")
        log.info('RA/DEC center:: '+radec_center.to_string())
    else:
        msg = '*** FATAL ERROR: RA/DEC center not found in '+solve_out
        log.error(msg)
        raise ValueError(msg)
    # search for FOV
    fovsrch = re.search(r"Field size: (\d+\.\d+)\s+x\s+(\d+\.\d+)\s+degrees", lines)
    if fovsrch:
        # Extract the width and height as floats
        fov_width = float(fovsrch.group(1)) * u.deg
        fov_height = float(fovsrch.group(2)) * u.deg
        log.info('FOV: '+str(fov_width)+' x '+str(fov_height))
    else:
        msg = '*** FATAL ERROR: FOV not found in '+solve_out
        log.error(msg)
        raise ValueError(msg)
    return (radec_center,[fov_width,fov_height])

def launch_astrometry_frame(frame_file,opth='./',read=False,
                            cfg_astmry='conf/astrometry.cfg',
                            submit=True):
    """Performs astrometry sovling from an image frame.
    
    Parameters
    ----------
    frame_file : string
        Frame file name.
    opth : string, optional
        Output directory name. Default is './'.
    read : boolean, optional.
        If True, data are red from file instead of computed. Useful
        for debug purpose only and/or to speed up the process. Default is False.
    cfg_astmry : string, optional
        Astrometry.net configuration file. Default is 'conf/astrometry.cfg'.
    submit : boolean, optional
        If False, the solve-field scrit is not run. Useful for debug purpose.
        Default is True.
    
    Returns
    -------
    wcs_obj : astropy.wcs.WCS object.
        World Coordinate System object.
        It contains all the astrometric transformation info.
    radec_center_c : astropy.coordinates.SkyCoord object.
        Updated image center sky coordinates.
    fov_sz : list of 2 astropy.units.Quantity objects.
        Size of field of view: width and height, in [deg].
    list_astrofile : dict
        List of all solve-field output files:
            'hdr': header,
            'xyls':xyls,
            'axy':axy,
            'match':match,
            'rdls':rdls,
            'corr':corr,
            'pnm':pnm,
            'new':new,
            'solved':solved,
            'fits':fits_file
        
    """
    global config
    try:
        dwnsmpl = config['CAMERA']['dwnsmpl']
    except:
        dwnsmpl = '1'
    # make output file names
    root = os.path.basename(frame_file).split('.')[0]
    hdrfile = opth+root+'-header.txt'
    xyls = hdrfile.replace('-header.txt','-xyls.fits')
    axy = hdrfile.replace('-header.txt','-axy.fits')
    match = hdrfile.replace('-header.txt','-match.fits')
    rdls = hdrfile.replace('-header.txt', '-rdls.fits')
    corr = hdrfile.replace('-header.txt','-corr.fits')
    pnm = hdrfile.replace('-header.txt','-pnm.bin')
    new = hdrfile.replace('-header.txt','-calib.fits')
    solved = hdrfile.replace('-header.txt','-solved')
    fits_file = opth+root+'.fits'
    solve_out = hdrfile.replace('-header.txt','-solved.txt')
    list_astrofile = {'hdr':hdrfile,
                      'xyls':xyls,
                      'axy':axy,
                      'match':match,
                      'rdls':rdls,
                      'corr':corr,
                      'corrdat':corr.replace('.fits','.dat'),
                      'pnm':pnm,
                      'new':new,
                      'solved':solved,
                      'fits':fits_file}
    # remove header file if exist to make sure no old version is considered.
    if submit and os.path.exists(hdrfile):
        log.debug('Removing hdr file: '+hdrfile)
        os.remove(hdrfile)
    # get zidth and height of the image in pixels
    with Image.open(frame_file) as img:
        width, height = img.size
    
    # convert frame into fits file
    if not (frame_file in fits_file):
        cmd = 'convert '+frame_file+' '+fits_file
        log.debug('Converting '+frame_file+' into '+fits_file)
        os.system(cmd)
    if read:
        wcs_header = fits.Header.fromfile(hdrfile,sep='\n',endcard=False,padding=False)
    else: 
        astmry_cmd = '/usr/bin/solve-field  --config '+cfg_astmry+\
            ' -D '+ opth +' --overwrite '+\
            ' --index-xyls '+xyls + \
            ' --axy ' + axy + \
            ' --match ' + match + \
            ' --corr ' + corr + \
            ' --rdls ' + rdls + \
            ' --wcs ' + hdrfile + \
            ' --pnm ' + pnm + \
            ' -S ' + solved + \
            ' -N ' + new + \
            ' -z ' + dwnsmpl + \
            ' ' + fits_file + \
            ' > ' + solve_out
        if submit:
            log.info('Submitting Astrometry.net command: '+astmry_cmd)
            os.system(astmry_cmd)
        else:
            log.warning('Astrometry.net command: '+astmry_cmd+' NOT submitted on purpose')
        # check output wcs header
        if not os.path.exists(hdrfile):
            log.error('*** Astrometry failed for '+fits_file)
        # read header file
        wcs_header = fits.Header.fromfile(hdrfile)
    
    # update RA/DEC center and fov
    (radec_center,fov_sz) = get_fov_from_solve(solve_out)
    
    # create wcs object
    wcs_obj = WCS(wcs_header)
    #log.debug(wcs_obj)
    
    # save corr file into dat file
    fits2dat(corr,sort='field_ra')
        
    return (wcs_obj,radec_center,fov_sz,list_astrofile)


def make_astrometry_data_from_bb(trk_data,bb_frame,mag_data,json_data,img_width,img_height,frame,wcs,opth='./',root='',avgflx=0,read=False):
    """Make astrometry data Table.

    Parameters
    ----------
    trk_data : astropy.table.QTable object.
        FMDT tracking output data, restricted to the considered meteor.
    bb_frame : astropy.table.QTable object.
        FMDT bb output data, restricted to one frame.
    mag_data : astropy.table.QTable object.
        FMDT magnitude output data.
    json_data : dict
        FMDT json output data.
    img_width : int
        Image width in pixels.
    img_height : int
        Image height in pixels.
    frame : int
        Meteor frame number. 
    wcs : astropy.wcs.WCS object.
        World coordinate system.
    opth : string, optional
        Output directory name. Default is './'.
    root : string, optional
        Output file root name. Default is ''.    
    avgflx : int, optional
        Average the star flux over plus and minus avgflx frames.
        Default is 0 (=no average).
    read : boolean, optional.
        If True, astrometry data are red from file instead of computed. Useful
        for debug purpose only and/or to speed up the process. Default is False.
    
    Returns
    -------
    astrometry_data : astropy.table.QTable object.
        Astrometry data: X, Y, Flux.
    fits_file : string
        Output fits file containing the data.

    """
    log.info('Make astrometry data for frame # '+str(frame))
    # make output file name
    frame_idstr = f"{frame:05d}"
    outfile = opth+root+'star_astrometry_'+str(frame_idstr)+'.dat'
    # read the data or make the data
    if read:
        log.info('Reading astrometry data from '+outfile)
        astrometry_data = QTable.read(outfile,format='ascii.fixed_width_two_line')
    else:
        astrometry_data = QTable(names=['X','Y','RA','DEC','Flux'])
        astrometry_data['X'].info.format = '7.2f'
        astrometry_data['Y'].info.format = '7.2f'
        astrometry_data['RA'].info.format = '7.3f'
        astrometry_data['DEC'].info.format = '5.2f'
        astrometry_data['Flux'].info.format = '7.2f'
        # loop over the stars
        for bb in bb_frame:
            log.debug('*** Considering object # '+str(bb['id']))
            #log.debug('bb: '+str(bb))
            trk_obj = trk_data[trk_data['id']==bb['id']]
            obj_type = trk_obj['type']
            #log.debug('trk_data for obj '+str(bb['id'])+' : '+str(trk_obj))
            if obj_type=='noise':
                log.info('=== Skip noise')
                continue
            if obj_type=='meteor':
                log.info('=== Skip meteor')
                continue
            # retrieve first and last frame where the star was detected
            frames = np.arange(trk_obj['f_b'],trk_obj['f_e']+1,1)
            frame_loc = np.where(frames==frame)[0][0]
            #log.debug('frames: '+str(frames))
            # log.debug('frame_loc: '+str(frame_loc))
            # log.debug('trk_obj[f_b]: '+str(trk_obj['f_b']))
            # log.debug('trk_obj[f_e]: '+str(trk_obj['f_e']))
            # retrieve star flux
            flux = mag_data[bb['id']]['data'][frame_loc]
            if avgflx>1:
                f_b = np.max([1,frame_loc-avgflx])-1 # because python index starts at 0
                f_e = np.min([len(frames),frame_loc+avgflx])
                log.debug('Frame average from: '+str(f_b)+' to '+str(f_e))
                log.debug('flux values: '+str(mag_data[bb['id']]['data'][f_b:f_e]))
                mag_array = np.array(mag_data[bb['id']]['data'][f_b:f_e])
                non_zero_flux = mag_array[mag_array != 0]
                flux = np.median(non_zero_flux)
                log.debug('median(lux): '+str(flux))
                # log.debug('flux averaged over '+str(f_e-f_b+1)+' frames: '+str(flux))
                # log.debug('mag_data[id]: '+str(mag_data[bb['id']]))
            else:
                flux = mag_data[bb['id']]['data'][frame_loc] * u.adu
                log.debug('flux (no average): '+str(flux))
            log.debug('flux of star # '+str(bb['id'])+' at frame '+str(frames[frame_loc])+' : '+str(flux))
            
            # get exact location of star in frame
            xc = json_data[str(frame)]['RoIs'][str(bb['id'])]['x']
            yc = json_data[str(frame)]['RoIs'][str(bb['id'])]['y']
            # compute sky coordinates
            radec = wcs.pixel_to_world(xc,img_height.value-yc) # because fits convetion different from FMDT
            astrometry_data.add_row([xc,img_height.value-yc,radec.ra,radec.dec,flux])
        # rearrange data for astrometry.net service
        astrometry_data.sort('Flux',reverse=True)
        #astrometry_data.sort('RA')
        log.debug('astrometry_data '+str(astrometry_data))
        log.info('There are '+str(len(astrometry_data))+' astrometry data')
        astrometry_data.write(outfile,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('astrometry data saved in '+outfile)
    return astrometry_data


def make_astrometry_data_from_json(json_data,img_width,img_height,frame,wcs,opth='./',root='',avgflx=0,read=False):
    """Make astrometry data Table.

    Parameters
    ----------
    json_data : dict
        FMDT json output data.
    img_width : int
        Image width in pixels.
    img_height : int
        Image height in pixels.
    frame : int
        Meteor frame number. 
    wcs : astropy.wcs.WCS object.
        World coordinate system.
    opth : string, optional
        Output directory name. Default is './'.
    root : string, optional
        Output file root name. Default is ''.    
    avgflx : int, optional
        Average the star flux over plus and minus avgflx frames.
        Default is 0 (=no average).
    read : boolean, optional.
        If True, astrometry data are red from file instead of computed. Useful
        for debug purpose only and/or to speed up the process. Default is False.
    
    Returns
    -------
    astrometry_data : astropy.table.QTable object.
        Astrometry data: X, Y, Flux.
    fits_file : string
        Output fits file containing the data.

    """
    log.info('Make astrometry data for frame # '+str(frame))
    # make output file name
    frame_idstr = f"{frame:05d}"
    outfile = opth+root+'_star_astrometry_'+str(frame_idstr)+'.dat'
    # read the data or make the data
    if read and os.path.exists(outfile):
        log.info('Reading astrometry data from '+outfile)
        astrometry_data = QTable.read(outfile,format='ascii.fixed_width_two_line')
    else:
        astrometry_data = QTable(names=['X','Y','RA','DEC','Flux'])
        astrometry_data['X'].info.format = '7.2f'
        astrometry_data['Y'].info.format = '7.2f'
        astrometry_data['RA'].info.format = '7.3f'
        astrometry_data['DEC'].info.format = '5.2f'
        astrometry_data['Flux'].info.format = '7.2f'
        # select data in frame
        data = json_data[str(frame)]
        for roiid in data['RoIs']:
            roi = data['RoIs'][roiid]
            log.debug('*** Considering ROI # '+str(roiid)+' rid: '+str(roi['rid']))
            tid = roi['tid'] # integer
            xc = roi['x']
            yc = roi['y']
            log.debug('tid: '+str(tid))
            log.debug('xc: '+str(xc))
            log.debug('yc: '+str(yc))
            # skip is tid=None
            if tid is None:
                log.info('=== Skip None tid')
                continue
            # skip noise and meteor
            if roi['otype']=='noise':
                log.info('=== Skip noise')
                continue
            if roi['otype']=='meteor':
                log.info('=== Skip meteor')
                continue
            # get beg/end frame number
            f_b = data['Tracks'][str(tid)]['fbeg']
            f_e = data['Tracks'][str(tid)]['fend']
            frames = np.arange(f_b,f_e+1,1)
            log.debug('f_b: '+str(f_b))
            log.debug('f_e: '+str(f_e))
            log.debug('frames: '+str(frames))
            frame_loc = np.where(frames==frame)[0][0]
            if avgflx>1:
                f_b_avg = np.max([1,frame_loc-avgflx])-1 # because python index starts at 0
                f_e_avg = np.min([len(frames),frame_loc+avgflx])
                log.debug('Frame average from: '+str(f_b_avg)+' to '+str(f_e_avg))
                flx_avg = []
                for f in np.arange(f_b_avg,f_e_avg+1,1):
                    for roiids in json_data[str(f)]['RoIs']:
                        rois = json_data[str(f)]['RoIs'][roiids]
                        if rois['tid']==tid:
                            flx = rois['mag']
                            flx_avg.append(flx)
                            log.debug('tid:'+str(tid)+' frame: '+str(f)+' mag:'+str(flx))
                if len(flx_avg)<1:
                    msg = '*** FATAL ERROR: impossible to get flux average'
                    log.error(msg)
                    raise ValueError(msg)
                flx_array = np.array(flx_avg)
                non_zero_flux = flx_array[flx_array != 0]
                flux = np.median(non_zero_flux)
                log.debug('flux (average over '+str(len(non_zero_flux))+' frames): '+str(flux))
            else:
                flux = roi['mag'] * u.adu
                log.debug('flux (no average): '+str(flux))
            # compute sky coordinates
            radec = wcs.pixel_to_world(xc,yc)
            # store data
            astrometry_data.add_row([xc,yc,radec.ra,radec.dec,flux])
        # rearrange data for astrometry.net service
        astrometry_data.sort('Flux',reverse=True)
        log.debug('astrometry_data '+str(astrometry_data))
        log.info('There are '+str(len(astrometry_data))+' astrometry data')
        astrometry_data.write(outfile,format='ascii.fixed_width_two_line',overwrite=True)
        log.info('astrometry data saved in '+outfile)
    return astrometry_data


def make_astrometry_data_from_corr(corrdatfile,frame_file):
    """Make astrometry data from the solve-field corr output file converted into dat.

    Parameters
    ----------
    corrdatfile : string
        corr-type file, output by solve-field script, converted into dat file.

    Returns
    -------
    astrometry_data : astropy.table.QTable object.
        Astrometry data containing: X, Y, RA, DEC, Flux.

    """
    log.info('Make astrometry data')
    outfile = corrdatfile.replace('-corr','-astrometry')
    # read file
    astrometry_data = QTable.read(corrdatfile,format='ascii.fixed_width_two_line')
    # rename and clean columns
    astrometry_data.rename_column('field_x','X')
    astrometry_data.rename_column('field_y','Y')
    astrometry_data.rename_column('FLUX','Flux')
    astrometry_data.rename_column('index_ra','RA')
    astrometry_data.rename_column('index_dec','DEC')
    astrometry_data.remove_columns(['index_x','index_id','field_ra','field_dec','field_id','match_weight','BACKGROUND'])
    # reorganiwe the table
    astrometry_data = astrometry_data[['X','Y','RA','DEC','Flux']]
    # set format
    astrometry_data['X'].info.format = '7.2f'
    astrometry_data['Y'].info.format = '7.2f'
    astrometry_data['RA'].info.format = '7.3f'
    astrometry_data['DEC'].info.format = '5.2f'
    astrometry_data['Flux'].info.format = '7.2f'
    astrometry_data['RA'].info.unit = u.deg
    astrometry_data['DEC'].info.unit = u.deg   
    # correct the flux for consistency with meteor flux measurement
    xy_stars = list(zip(astrometry_data['X'], astrometry_data['Y']))
    astrometry_data['Flux'] = get_object_flux(frame_file,xy_stars) * u.adu
    # sort data
    astrometry_data.sort('Flux',reverse=True)
    #astrometry_data.sort('RA')
    log.debug('astrometry_data '+str(astrometry_data))
    log.info('There are '+str(len(astrometry_data))+' astrometry data')
    astrometry_data.write(outfile,format='ascii.fixed_width_two_line',overwrite=True)
    log.info('astrometry data saved in '+outfile)
    return astrometry_data


def get_object_flux(fits_file,imgcoo,obj_rad=5,in_rad=10,out_rad=15):
    """
    Get object flux.

    Parameters
    ----------
    fits_file : string
        Frame file name, in fits format.
    imgcoo : tuple of 2 float
        Image coordinate of the object.
    obj_rad : float, optional
        Object cirle radius in [pixels]. The default is 5 pixels.
    in_rad : float, optional
        Inner cirle radius in [pixels]. The default is 10 pixels.
    out_rad : float, optional
        Outer cirle radius in [pixels]. The default is 15 pixels.

    Returns
    -------
    flux : float
        Background subtracted object flux.

    """
    # read image data
    data = fits.open(fits_file)[0].data
    # create aperture and annulus object
    aperture = CircularAperture(imgcoo, r=obj_rad)
    annulus_aperture = CircularAnnulus(imgcoo, r_in=in_rad, r_out=out_rad)
    # compute stats
    aperstats = ApertureStats(data, annulus_aperture)
    # retrieve background value
    bkg_mean = aperstats.mean
    # get aperture data as astropy,table.Table
    phot_table = aperture_photometry(data, aperture)
    # compute aperture area and total background
    aperture_area = aperture.area_overlap(data)
    total_bkg = bkg_mean * aperture_area
    # compute background subtracted flux
    phot_bkgsub = phot_table['aperture_sum'] - total_bkg
    # reorganise the data
    if len(phot_bkgsub)>1:
        flux = phot_bkgsub
    else:
        flux = phot_bkgsub[0]
    # aberrant value
    # if flux<=0.0:
    #     msg = '*** Aberrant value of Flux:'+str(flux)
    #     log.warning(msg)
    #     flux = 0.0
    # # log
    log.debug('bkg_mean: '+str(bkg_mean))
    log.debug('aperture_area: '+str(aperture_area))
    log.debug('total_bkg: '+str(total_bkg))
    log.debug('phot_bkgsub: '+str(phot_bkgsub))
    log.debug('flux: '+str(flux))
    return flux

def fmdt_solve(config_file):
    """Launch FMDT solve astrometry and photometry process.

    Parameters
    ----------
    config_file : string
        configuration file.
    
    Returns
    -------
    None.

    """
    global config
    # read configuration file
    (config,
     [img_width,img_height],
     [fov_width,fov_height],
     img_scale,
     fps,
     time_start,
     LimMag,band,
     avgflx,
     match_tol_fct,
     read_astrometry,
     read_photometry,
     opth,
     fpth,
     fmt,
     root,
     log_dir,
     trk_meteor_min,
     submit) = read_config(config_file)
    # read FMDT output files
    try:
        trk_data,bb_data,flx_data,json_data = fmdt_read.read_fmdt_outputs(
                                                        config['FMDT']['track_file'],
                                                        config['FMDT']['bb_file'],
                                                        config['FMDT']['mag_file'],
                                                        config['FMDT']['json_file'])
    except:
        log.error('*=*=*= Unable to read FMDT output data => end of fmdt_solve')
        return
    # select meteor track data
    met_trk = trk_data[trk_data['type']=='meteor']
    log.info('There are '+str(len(met_trk))+' meteors detected in '+config['FMDT']['track_file'])

    # for each meteor: perform astrometry
    for met in met_trk:
        # get track id
        met_id = met['id']
        log.info('*** Processing meteor tid # '+str(met_id)+' detected in frames '+str(met['f_b'])+' to '+str(met['f_e']))
        
        # double check object type
        met_flx_type = flx_data[met_id]['type']
        if not met_flx_type=='meteor':
            msg = 'FATAL ERROR: flx_data not a meteor type'
            log.error(msg)
            log.error('flx_data: '+str(flx_data[met_id]))
            raise ValueError(msg)
        
        # retrieve all meteor flux data in ADU
        met_flx = flx_data[met_id]['data']
        # number of data
        nb_frame = len(met_flx)
        # check number of data
        nb_frame2 = met['f_e']+1-met['f_b']
        if not nb_frame==nb_frame2:
            msg = '*** FATAL ERROR: nb_frame='+str(nb_frame)+' different from nb_frame2='+str(nb_frame2)
            log.error(msg)
            raise ValueError(msg)
        log.debug('Meteor all flux data: '+str(met_flx))
        
        # make meteor output QTable
        met_data = QTable(names=['Time','frame','X','Y','RA','DEC','Flux','Mag','Mag_err'],
                          dtype=['object','int','float','float','float','float','float','float','float'])
        
        # loop over each meteor frame
        for frame in np.arange(met['f_b'],met['f_e']+1,1,dtype='int'):
            log.debug('Considering frame #: '+str(frame))
            # original frame file
            frame_file = frame2filename(config['USER']['frame_dir'],
                                        config['USER']['root'],
                                        frame,
                                        fmt='%5d.fits',met_id=met_id,
                                        opth=config['USER']['frame_dir'])
            # compute time of frame
            frame_time = time_start + TimeDelta(float(frame)/fps)
            # try to get more accurate time
            log.debug('frame_file: '+frame_file)
            try:
                frame_time = path2time(frame_file)
            except:
                msg = 'unable to get frame time'
                log.error(msg)
                raise ValueError(msg)
            # select objects in current frame
            bb_obj = bb_data[bb_data['frame']==frame]
            log.debug('There are '+str(len(bb_obj))+' objects in frame '+str(frame))
            log.debug('bb_obj='+str(bb_obj))
            # retrieve meteor bb data
            bb_met = bb_obj[bb_obj['id']==met_id]
            log.debug('bb_met = '+str(bb_met))

            # perform frame astrometry with local astrometry.net software
            try:
                (wcs_obj,
                 radec_center,
                 [fov_width,
                  fov_height],
                 list_astrofile) = launch_astrometry_frame(frame_file,opth=fpth,
                                                           read=read_astrometry,
                                                           cfg_astmry=config['USER']['astrometry_cfg'],
                                                           submit=submit)
            except:
                log.error('*** Impossible to perform astrometry for frame: '+frame_file)
                continue
            # get meteor image coordinates: warning: FMDT convention different from fits standards!
            met_x = bb_met['Xc'][0]
            met_y = img_height.value - bb_met['Yc'][0]
            log.debug('bb_met[x/y]='+str((bb_met['Xc'][0],bb_met['Yc'][0])))
            log.debug('met_x/y='+str((met_x,met_y)))
            # compute meteor sky coordinates
            radec_met = wcs_obj.pixel_to_world(met_x,met_y)
            log.debug('Meteor RADEC: '+radec_met.to_string())
            # get meteor flux 
            met_flux = get_object_flux(frame_file,(met_x,met_y)) * u.adu
            # compute other meteor quantities if meteor flux not zero
            if int(met_flux.value)==0:
                log.warning('met_flux=0 -> next frame')
                continue
            # retrieve star positions
            # astrometry_data = make_astrometry_data_from_bb(trk_data,
            #                                                bb_obj,
            #                                                flx_data,
            #                                                json_data,
            #                                                img_width,
            #                                                img_height,
            #                                                frame,
            #                                                wcs_obj,
            #                                                avgflx=avgflx,
            #                                                opth=fpth,
            #                                                root=root,
            #                                                read=read_astrometry)
            # astrometry_data = make_astrometry_data_from_json(json_data,
            #                                                  img_width,
            #                                                  img_height,
            #                                                  frame,
            #                                                  wcs_obj,
            #                                                  avgflx=avgflx,
            #                                                  opth=opth,
            #                                                  root=root,
            #                                                  read=read_astrometry)
            
            # make astrometry data: sar image and sky position
            astrometry_data = make_astrometry_data_from_corr(list_astrofile['corrdat'],
                                                             frame_file)
            # check if number of star is high enough for astrometry
            star_num = len(astrometry_data)
            num_min = int(int(config['PROCESS']['min_star_nb']))
            if (star_num<num_min):
                log.warning('Detected Stars number ='+str(star_num)+' < num_min='+str(num_min)+' -> next frame')
                continue
            # match detected stars with catalog stars
            matched_data = match_stars(astrometry_data,
                                       wcs_obj,
                                       radec_center,
                                       [fov_width,fov_height],
                                       maxsep=img_scale*u.pix*match_tol_fct,
                                       LimMag=LimMag,
                                       frame=frame,
                                       opth=fpth,
                                       root=root,
                                       nmatch_min=int(config['PROCESS']['min_star_nb']))
            # loop if too low matched stars
            if len(matched_data)<num_min:
                log.warning('# of matched stars for mag computation: '+str(len(matched_data))+' < num_min='+str(num_min)+' -> next frame')
                continue            
            
            # make meteor photometry
            met_mag, met_mag_err = make_photometry_fit(matched_data,
                                          met_flux,
                                          met_id=met_id,
                                          band=band,
                                          frame=frame,
                                          opth=fpth,
                                          root=root)
    
            # add data to meteor table
            met_data.add_row([frame_time,
                              frame,
                              bb_met['Xc'][0],
                              bb_met['Yc'][0],
                              radec_met.ra.to('deg'),
                              radec_met.dec.to('deg'),
                              int(met_flux.value),met_mag,met_mag_err])

        # save and plot meteor data
        if len(met_data)<1:
            log.warning('Not enough Meteor data for meteor id='+str(met_id)+' -> next meteor')
            continue
        else:
            save_n_plot_meteor(met_data,met_id,LimMag=LimMag,opth=opth,root=root)
        
    log.info('*** All done.',)
    return




if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='fmdt-solve arguments.')
    parser.add_argument('-c',default='./conf/config.in',help='configuration file. Default is: ./conf/config.in')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.c
    
    # launch 
    fmdt_solve(config_file)
    
else:
    log.debug('successfully imported')

