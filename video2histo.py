#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 09:01:40 2025

@author: ccolomer
"""
import argparse
import ffmpeg
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import io

def extract_frames(video_path, num_frames=10):
    frames = []
    # Use ffmpeg to get frames from the video
    probe = ffmpeg.probe(video_path, v='error', select_streams='v:0', show_entries='stream=width,height,r_frame_rate')
    fps = eval(probe['streams'][0]['r_frame_rate'])  # Get frames per second

    # Use ffmpeg to extract frames
    out, _ = (
        ffmpeg
        .input(video_path)
        .output('pipe:', format='rawvideo', pix_fmt='rgb24', vframes=num_frames)
        .run(capture_stdout=True, capture_stderr=True)
    )

    # Convert raw output to images
    frame_size = probe['streams'][0]['width'] * probe['streams'][0]['height'] * 3  # width * height * 3 (RGB)
    frames = np.frombuffer(out, np.uint8).reshape((-1, int(probe['streams'][0]['height']), int(probe['streams'][0]['width']), 3))

    return frames

def compute_histograms(frames):
    histograms = []

    # Iterate over frames and compute the histogram
    for frame in frames:
        image = Image.fromarray(frame)
        gray_image = image.convert('L')  # Convert to grayscale
        histogram = np.array(gray_image.histogram())  # Get the histogram
        histograms.append(histogram)

    return histograms

def plot_mean_histogram(histograms):
    # Compute the mean histogram
    mean_histogram = np.mean(histograms, axis=0)

    # Plot the histogram
    plt.plot(mean_histogram)
    plt.title("Mean Histogram of First 10 Frames")
    plt.xlabel("Pixel Value")
    plt.ylabel("Frequency")
    plt.yscale('log')  # Apply logarithmic scale to the y-axis
    plt.show()

def main(video_path,num_frames=10):
    frames = extract_frames(video_path, num_frames=num_frames)
    histograms = compute_histograms(frames)
    plot_mean_histogram(histograms)

if __name__ == "__main__":
    # parse arguments
    parser = argparse.ArgumentParser(description='Extract first n frames from video and plot mean histogram.')
    parser.add_argument('-i',help='Input video file.')
    parser.add_argument('-n',default=10,help='Number of frame to average. Default is 10.')
    args = parser.parse_args()
    
    # retrieve arguments
    video_file = args.i
    n = args.n
    
    # launch main program
    main(video_file,num_frames=n)
    
